/**
 * Created by GSN on 6/10/2015.
 */

//==============================================================================
// create and cache animations
//==============================================================================
fr.createAnimationById = function(key, object) {
    return fr.createAnimation("res/animations/" + key, key, object);
};

fr.playAnimationRepeatForever = function(keyAnimOrAnim, action){
    if(action === undefined) action = "run";

    //ZLog.debug("play animation = %s", keyAnimOrAnim);
    if(cc.isString(keyAnimOrAnim)){
        var anim = fr.createAnimationById(keyAnimOrAnim);
        anim.getAnimation().gotoAndPlay(action, -1, -1, 0);

        return anim;
    }
    else{
        keyAnimOrAnim.getAnimation().gotoAndPlay(action, -1, -1, 0);
        //ZLog.debug("playAnimationRepeatForever | " + keyAnimOrAnim);
        return keyAnimOrAnim;
    }
};

/**
 *
 * @param keyAnim
 * @param action
 */
fr.playAnimationOnce = function(keyAnim, action){
    if(action === undefined) action = "run";

    var anim = fr.createAnimationById(keyAnim);
    anim.retain();
    anim.getAnimation().gotoAndPlay(action, -1, -1, 1);
    anim.setCompleteListener(function(sender){
        sender.removeFromParent(true);
        //fr.addAnimationCache(keyAnim, sender);
    });

    return anim;
};

/**
 *
 */
fr.addAnimationCache = function(key, anim){
    if(!fr.createAnimation.cache.hasOwnProperty(key)){
        fr.createAnimation.cache[key] = [];
    }

    //ZLog.debug("---> add into animation cache: key =  %s", key);
    fr.createAnimation.cache[key].push(anim);
},

/**
 *
 */
fr.cleanUpAnimationCache = function(){
    var cache = fr.createAnimation.cache;
    for(var key in cache){
        for(var i = 0; i < cache[key].length; ++i){
            //ZLog.debug("---> clean up animation cache: key =  %s", key);
            cache[key][i].removeFromParent(true);
        }
        cache[key].splice(0);
    }
},

/**
 *
 */
fr.createAnimation = function(folderPath, key, object)
{
    if(fr.createAnimation.cache[key] != null && fr.createAnimation.cache[key].length > 0){
        //ZLog.debug("---> get from animation cache: key =  %s", key);
        return fr.createAnimation.cache[key].splice(0, 1)[0];
    }
    else{
        fr.loadAnimationData(folderPath, key, object);
        return db.DBCCFactory.getInstance().buildArmatureNode(key);
    }
};
fr.createAnimation.cache = {};

//==============================================================================
//  load and unload animations
//==============================================================================
fr.loadAnimationDataByKey = function(key, object){
    fr.loadAnimationData("res/animations/" + key, key, object);
};

fr.loadAnimationData = function(folderPath, key, object)
{
    if(object != undefined && object != null)
    {
        if(object.listAnimationLoaded == undefined)
        {
            object.listAnimationLoaded = {};
        }
        if(key in object.listAnimationLoaded)
            return;
        object.listAnimationLoaded[key] = key;
    }
    db.DBCCFactory.getInstance().loadTextureAtlas(folderPath + "/texture.plist", key);
    db.DBCCFactory.getInstance().loadDragonBonesData(folderPath + "/skeleton.xml", key);
};

fr.unloadAllAnimationData = function(object)
{
    if(object.listAnimationLoaded == undefined)
    {
        return;
    }

    for(var keyStored in object.listAnimationLoaded)
    {
        db.DBCCFactory.getInstance().removeTextureAtlas(keyStored,false);
    }
    object.listAnimationLoaded = {};
};