/**
 * Created by KienVN on 5/22/2015.
 */

function newObject(clazz){
    return new (clazz.bind.apply(clazz, arguments));
}

fr.OutPacket.extend = cc.Class.extend;
fr.InPacket.extend = cc.Class.extend;

ccui.LoadingBar.prototype.setZPercent = function(percent){
    if(this.basePercent === undefined){
        var capInsets = this.getCapInsets();
        this.basePercent = Math.ceil(100 * (capInsets.x * 2 + capInsets.width) / this.getContentSize().width);
    }

    var basePercent = this.basePercent;

    if (0 < percent && percent < this.basePercent) percent = this.basePercent;
    if(percent > 100) percent = 100;

    this.setVisible(percent > 0);
    this.setPercent(percent);
};

ccui.LoadingBar.prototype._setZPercent = function(percent){
    this.setZPercent(percent);
};

ccui.LoadingBar.prototype.runPercent = function(from, to, duration, callback){
    if(this.idInterval === undefined) this.idInterval = -1;
    if(this.uiPercent === undefined) this.uiPercent = 0;
    if(this.runPercentCb === undefined) this.runPercentCb = null;
    if(this.idInterval > -1) clearInterval(this.idInterval);

    this.uiPercent = from;
    this.runPercentCb = callback;

    var timeStep = 10;
    var numOfSteps = (duration * 1000) / timeStep;
    var percentStep = (to - from) / numOfSteps;
    this.idInterval = setInterval(function(){
        this.uiPercent += percentStep;

        this.setZPercent(this.uiPercent);
        if(this.uiPercent > to){
            clearInterval(this.idInterval);
            this.idInterval = -1;

            callback && callback();
            callback = null;
        }
    }.bind(this), timeStep);
};