/**
 * Created by bachbv on 1/30/2017.
 */

var Audio = {

    init: function(){
        this._soundIdList = {};
        this._mutingAudio = false;
        this._mutingSound = this._mutingAudio;
        this._mutingMusic = this._mutingAudio;
        this._curMusic = null;

        this.setMaxEffectsInstance(20);
        //this._preloadAudio(res_music);
        //this._preloadAudio(res_sound, false);
    },

    _preloadAudio: function(arrName, isMusic){
        if(isMusic === undefined || isMusic){
            for(var i = 0; i < arrName.length; ++i){
                cc.audioEngine.preloadMusic(arrName[i]);
            }
        }
        else{
            for(i = 0; i < arrName.length; ++i){
                cc.audioEngine.preloadEffect(arrName[i]);
            }
        }
    },

    isMutingAudio: function(){
        return this._mutingAudio;
    },

    setMutingAudio: function(b){
        if(this._mutingAudio == b) return;

        this._mutingAudio = b;
        this.setMutingSound(b);
        this.setMutingMusic(b);
    },

    isMutingSound: function(){
        return this._mutingSound;
    },

    setMutingSound: function(b){
        this._mutingSound = b;
    },

    isMutingMusic: function(){
        return this._mutingMusic;
    },

    setMutingMusic: function(b){
        this._mutingMusic = b;

        if(b){
            this.stopMusic();
        }
        else{
            if(this._curMusic.length > 0){
                this.playMusic(this._curMusic);
            }
        }
    },


    playMusic: function (url, loop) {
        this._curMusic = url;

        if(this.isMutingMusic()) return;
        loop = loop || true;
        cc.audioEngine.playMusic(url, loop);
    },

    stopMusic: function() {
        cc.audioEngine.stopMusic();
    },

    pauseMusic: function () {
        cc.audioEngine.pauseMusic();
    },

    resumeMusic: function () {
        cc.audioEngine.resumeMusic();
    },

    rewindMusic: function () {
        cc.audioEngine.rewindMusic();
    },

    // is background music playing
    isMusicPlaying: function () {
        if (cc.audioEngine.isMusicPlaying()) {
            ZLog.debug("background music is playing");
        }
        else {
            ZLog.debug("background music is not playing");
        }
    },

    playEffect: function (url, loop) {
        if(this.isMutingSound()) return;

        loop = loop || false;
        var soundId = cc.audioEngine.playEffect(url, loop);
        if(this._soundIdList[url] != soundId){
            this._soundIdList[url] = soundId;
        }
    },

    stopEffect: function (baseName) {
        if(this._soundIdList[baseName]){
            cc.audioEngine.stopEffect(this._soundIdList[baseName]);
        }
    },

    unloadEffect: function () {
        cc.audioEngine.unloadEffect("");
    },

    addMusicVolume: function () {
        cc.audioEngine.setMusicVolume(cc.audioEngine.getMusicVolume() + 0.1);
    },

    subMusicVolume: function () {
        cc.audioEngine.setMusicVolume(cc.audioEngine.getMusicVolume() - 0.1);
    },

    addEffectsVolume: function () {
        cc.audioEngine.setEffectsVolume(cc.audioEngine.getEffectsVolume() + 0.1);
    },

    subEffectsVolume: function () {
        cc.audioEngine.setEffectsVolume(cc.audioEngine.getEffectsVolume() - 0.1);
    },

    pauseEffect: function (baseName) {
        if(this._soundIdList[baseName]){
            cc.audioEngine.pauseEffect(this._soundIdList[baseName]);
        }
    },

    resumeEffect: function (baseName) {
        if(this._soundIdList[baseName]){
            cc.audioEngine.resumeEffect(this._soundIdList[baseName]);
        }
    },

    pauseAllEffects: function () {
        cc.audioEngine.pauseAllEffects();
    },

    resumeAllEffects: function () {
        cc.audioEngine.resumeAllEffects();
    },

    stopAllEffects: function () {
        cc.audioEngine.stopAllEffects();
    },

    setMaxEffectsInstance: function(num){
        cc.audioEngine._maxAudioInstance = num;
    },

    cleanUp: function(){
        this.stopAllEffects();
        this.stopMusic();

        for(var baseName in this._soundIdList){
            delete this._soundIdList[baseName];
        }
        this._soundIdList = null;
    },
};
