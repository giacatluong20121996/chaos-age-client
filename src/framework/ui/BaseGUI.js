/**
 * Created by bachbv on 1/16/2017.
 */

var UIKey = {
    AUTO_SCALE: "-auto-scale",
    DISABLE_PRESSED_SCALE: "-disable-pressed-scale",
    DISABLE_HOVER: "-disable-hover"
};

var GUIMgr = {
    _queue: [],
    numOfFullScreen: 0,

    isEmpty: function(){
        return this._queue.length == 0;
    },

    isInQueue: function(gui){
        return this._queue.indexOf(gui) >= 0;
    },

    getQueueLength: function(){
        return this._queue.length;
    },

    getQueue: function(){
        return this._queue;
    },

    push: function(gui){
        var index = this._queue.indexOf(gui);
        if(index < 0){
            this._queue.push(gui);
            if(gui.isFullScreen) {
                ++this.numOfFullScreen;
                sceneMgr.setVisibleLayersBehind(false);
            }
        }
    },

    remove: function(gui){
        var index = this._queue.indexOf(gui);
        if(index >= 0){
            this._queue.splice(index, 1);
            if(gui.isFullScreen) --this.numOfFullScreen;
        }

        if(this.numOfFullScreen <= 0) sceneMgr.setVisibleLayersBehind(true);
    },

    hideAllInScene: function(scene){
        if(scene === undefined) scene = sceneMgr.getCurrentScene();
        if(scene == null) return;

        var length = this._queue.length;
        var index = 0;
        this.numOfFullScreen = 0;
        for(var i = 0; i < length; ++i){
            if(getSceneOfGUI(this._queue[index]) == scene){
                this._queue[index].hideArrow(false);
            }
            else{
                ++index;
            }
        }
    },

    removeAll: function(){
        this._queue.splice(0);
    }
};

var BaseGUI = cc.Layer.extend({
    _className: "BaseGUI",

    ctor:function(){
        this._super();
        this.screenConfig = null;
        this._deepSyncChildren = 1;
        this._isRegistryTouchOutGUIEvent = false;
        this._guiRect = cc.rect(0, 0, 0, 0);
        this._listButtons = [];
        this._listEditBox = [];

        this.isFullScreen = false;
        this.languageListenerEvent = null;
        this.btnOnHighlight = -1;
        this.languageDirty();
    },

    addToListButton: function(btn) {
        this._listButtons.push(btn);
    },

    addToListEditBox: function(ed){
        if(ed && ed instanceof cc.EditBox){
            this._listEditBox.push({target: ed, state: ed.isVisible()});
        }
    },

    /**
     * check button contain point
     * @param btn
     * @param pos
     * @returns {boolean}
     */
    hasContain: function(btn, pos){
        var realPos = btn.getParent().convertToWorldSpace(btn.getPosition());
        var width = btn.getBoundingBox().width;
        var height = btn.getBoundingBox().height;
        var anchorPoint = btn.getAnchorPoint();

        if (!btn.isVisible() || !btn.getParent().isVisible()) return false;

        return (realPos.x - width * anchorPoint.x <= pos.x && realPos.x + width * (1 - anchorPoint.x) > pos.x
            && (realPos.y - height * anchorPoint.y <= pos.y && realPos.y + height * (1 - anchorPoint.y) > pos.y));
    },

    highlightButton: function(btn) {
        if(btn == null || (btn.customData && btn.customData.indexOf(UIKey.DISABLE_HOVER) > -1)) {
            return;
        }

        btn.setScale(1.07);
    },

    removeHighlightButton: function(btn) {
        if(btn) btn.setScale(1.0);
    },

    languageDirty: function(){
        this.isLanguageDirty = true;
    },

    localize: function(){
        this.isLanguageDirty = false;
    },

    updateLocalization: function(){
        if(this.isLanguageDirty){
            this.localize();
        }
    },

    addCustomEvent: function (eventName, funcCallBack) {
        this.languageListenerEvent = cc.EventListener.create({
            event: cc.EventListener.CUSTOM,
            eventName: eventName,
            callback: function (event) {
                funcCallBack(event);
            }.bind(funcCallBack)
        });
        cc.eventManager.addListener(this.languageListenerEvent, 1);
    },

    getClassName: function(){
        return this._className;
    },

    setDeepSyncChildren: function(deep){
        this._deepSyncChildren = deep;
    },

    onEnter: function(){
        this._super();

        if(this.languageListenerEvent == null){
            //this.addCustomEvent(LanguageMgr.langEventName, this.languageDirty.bind(this));
        }

        this.updateLocalization();
    },

    syncAllChildren:function(url, parent){
        this.screenConfig = ccs.load(url, "res/");
        this._rootNode = this.screenConfig.node;
        parent.addChild(this._rootNode);
        this._syncChildrenInNode(this._rootNode, 0);
    },

    doLayout: function(size){
        this._rootNode.setContentSize(size);
        ccui.helper.doLayout(this._rootNode);
    },

    alignCenter: function(){
        this.setContentSize(cc.winSize);
        if(this._rootNode){
            this._rootNode.setPosition(GV.VISIBALE_SIZE.width >> 1, GV.VISIBALE_SIZE.height >> 1);
        }
        else{
            cc.warn("BaseGUI.alignCenter(): this._rootNode = null");
        }
    },

    _syncChildrenInNode: function(node, deep){
        if(deep >= this._deepSyncChildren) return;

        var allChildren = node.getChildren();
        if(allChildren === null || allChildren.length == 0) return;

        var childName;
        for(var i = 0; i < allChildren.length; i++) {
            childName = allChildren[i].getName();

            if(childName in this && this[childName] === null){
                this[childName] = allChildren[i];
                this.handleUIByType(allChildren[i]);
                this.handleCustomData(allChildren[i]);
            }
            this._syncChildrenInNode(allChildren[i], deep + 1);
        }
    },

    handleUIByType: function(node){
        var name = node.getName();
        var newUI = null;
        if(UIUtils.isButton(name)){
            node.setPressedActionEnabled && node.setPressedActionEnabled(true);
            node.addTouchEventListener && node.addTouchEventListener(this._onTouchUIEvent, this);
            this.addToListButton(node);
        }
        else if(UIUtils.isCheckbox(name)){
            node.setPressedActionEnabled && node.setPressedActionEnabled(true);
            node.addTouchEventListener && node.addTouchEventListener(this._onTouchUIEvent, this);
        }
        else if(UIUtils.isControlSwitch(name)){
            newUI = UIUtils.toControlSwitch(node);
            newUI.addTargetWithActionForControlEvents(this, this.onControlSwitchChange, cc.CONTROL_EVENT_VALUECHANGED);
        }

        // remove old ui and updateCamera refer to new ui
        if(newUI != null) {
            node.getParent().addChild(newUI);
            node.removeFromParent(true);

            this[newUI.getName()] = newUI;
        }
    },

    handleCustomDataByName: function(name){
        var node = ccui.helper.seekWidgetByName(this._rootNode, name);
        if(node){
            this.handleCustomData(node);
        }
    },

    handleCustomData: function(node){
        if(node.customData){
            var data = node.customData;

            if(data && data.indexOf(UIKey.DISABLE_PRESSED_SCALE) > -1){
                node.setPressedActionEnabled && node.setPressedActionEnabled(false);
            }
        }

        return node;
    },

    isTouchOutOfGUI: function(touchedPos){
        if(this._isRegistryTouchOutGUIEvent && !cc._rectEqualToZero(this._guiRect)){
            return cc.rectContainsPoint(this._guiRect, touchedPos);
        }

        return false;
    },

    handleTouchOutGUI: function(){
        // override me

        // default: hideArrow this GUI
        this.setVisible(false);
    },

    _onTextFieldEvent: function(textField, type){
        switch (type){
            case ccui.TextField.EVENT_ATTACH_WITH_IME:
                break;
            case ccui.TextField.EVENT_DETACH_WITH_IME:
                break;
            case ccui.TextField.EVENT_INSERT_TEXT:
                break;
            case ccui.TextField.EVENT_DELETE_BACKWARD:
                break;
            default:
                break;
        }
    },

    _onTouchUIEvent:function(sender, type){
        switch (type){
            case ccui.Widget.TOUCH_BEGAN:
                this.onTouchUIBeganEvent(sender);
                break;
            case ccui.Widget.TOUCH_MOVED:
                this.onTouchUIMovedEvent(sender);
                break;
            case ccui.Widget.TOUCH_ENDED:
                this.onTouchUIEndEvent(sender);
                break;
            case ccui.Widget.TOUCH_CANCELED:
                this.onTouchUICancelEvent(sender);
                break;
        }
    },

    _showFog: function(){
        sceneMgr.showFog(this, this.getClassName());
    },

    _hideFog: function(){
        sceneMgr.hideFog(this);
    },

    //setVisibleLayersBehind: function(b){
    //    var currentScene = sceneMgr.getCurrentScene();
    //    if(currentScene != null){
    //        currentScene.getLayer(GV.LAYERS.BG).setVisible(b);
    //        currentScene.getLayer(GV.LAYERS.GAME).setVisible(b);
    //        currentScene.getLayer(GV.LAYERS.EFFECT).setVisible(b);
    //    }
    //},

    showFromPos: function(pos, hasEffect, showFog){
        if(hasEffect === undefined) hasEffect = true;
        if(showFog === undefined) showFog = true;

        var delta = cc.pSub(pos, this._rootNode.getPosition());
        this.setPosition(delta);
        this.show(hasEffect, showFog);

        if(hasEffect){
            var action = cc.moveTo(0.2, cc.p(0, 0));
            this.runAction(action);
        }
    },

    show: function(hasEffect, showFog){
        this.updateLocalization();

        if(hasEffect === undefined) hasEffect = true;
        if(showFog === undefined) showFog = true;

        if(showFog){
            this._showFog();
        }

        GUIMgr.push(this);

        this.stopAllActions();
        this.setVisible(true);

        if(hasEffect){
            this.setScale(0.01);
            this.setOpacity(66);

            var speedShow = 0.2;
            var fadeIn = cc.fadeIn(speedShow);
            var scaleIn = cc.scaleTo(speedShow, 1.1, 1.1);
            var scaleOut = cc.scaleTo(speedShow * 0.5, 1.0, 1.0);

            this.runAction(cc.sequence(cc.spawn(fadeIn, scaleIn), scaleOut));
        }else{
            this.setScale(1);
            this.setOpacity(255);
        }
    },

    showAtCurrentScene: function(zOrder){
        if(zOrder === undefined) zOrder = 4;

        var curScene = sceneMgr.getCurrentScene();
        if(curScene) {
            var layer = curScene.getLayer(GV.LAYERS.GUI);
            if (this.parent != layer) {
                this.removeFromParent(false);
                layer.addChild(this, zOrder);
            }
        }

        this.show();
    },

    hide: function(hasEffect){
        if(hasEffect === undefined) hasEffect = true;

        if(!this.isVisible()){
            return;
        }

        GUIMgr.remove(this);

        this._hideFog();
        if(hasEffect){
            this.setOpacity(255);

            var speedShow = 0.15;
            var fadeOut = cc.fadeOut(speedShow);
            var scaleOut = cc.scaleTo(speedShow, 0.01, 0.01);

            this.runAction(
                cc.sequence(
                    cc.spawn(fadeOut, scaleOut),
                    cc.callFunc(function(sender){
                        sender.setVisible(false);
                    })
                )
            );
        }
        else{
            this.setVisible(false);
        }
    },

    addHotNewsDispatch: function(){
        this._hotNewsDispatch = true;
    },

    removeHotNewsDispatch: function(){
        this._hotNewsDispatch = true;
    },

    onTouchUIBeganEvent:function(sender){
        // override me
    },

    onTouchUIMovedEvent:function(sender){
        // override me
    },

    onTouchUIEndEvent:function(sender){
        // override me
    },

    onTouchUICancelEvent:function(sender){
        // override me
    },

    onHoverIn: function(sender){
        this.highlightButton(sender);
    },

    onHoverOut: function(sender){
        this.removeHighlightButton(sender);
    },

    onControlSwitchChange:function(sender, controlEvent){
        // override me
    },
});