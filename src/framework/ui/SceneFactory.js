/**
 * Created by bachbv on 1/16/2017.
 */

var SceneFactory = cc.Class.extend({

    ctor:function(){
        GV.SCENE_FACTORY = this;
        return true;
    },

    /**
     *
     * @param sceneId
     */
    createScreen:function(sceneId) {
        switch(sceneId){
            //case GV.SCENE_IDS.LOADING:
                //ZLog.debug("-> CREATE SCENE: LOADING");
                //return new SceneLoading();

            case GV.SCENE_IDS.LOGIN:
                ZLog.debug("-> CREATE SCENE: LOGIN");
                return new SceneLogin();
            case GV.SCENE_IDS.PLAY:
                return new PlayScene();
            case GV.SCENE_IDS.SHOP:
                return new LayerShop();
            case GV.SCENE_IDS.ITEM_SHOP:
                return new LayerItemShop();
        }
    },

    createTransition:function(scene, oldScreenId,_currentScreenId) {
         return new cc.TransitionFade(0.3, scene, cc.color(0, 0, 0, 255));
    }
});