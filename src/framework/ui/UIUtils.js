/**
 * Created by Tomorow on 4/22/2017.
 */

UI_PREFIX = {
    BUTTON:         "btn",
    CHECKBOX:       "cb",
    CONTROL_SWITCH: "cs",
    RICH_TEXT:      "rt",
    WEB_VIEW:       "wv",
    TABLE_VIEW:     "tv",
    EDIT_BOX:       "eb",
    HTML_TEXT:      "html",
};

UI_PARAMS_REQUIRE = {
    CONTROL_SWITCH: 6,
};
var UIUtils = {
    _controlSwitchDefault: ["bg_switch", "bg_switch_on", "bg_switch_off", "icon_switch", "UTM_SWISS_721_BLACK_CONDENSED", 25],

    isButton: function(name){
        return _.isString(name) && name.startsWith(UI_PREFIX.BUTTON);
    },

    isCheckbox: function(name){
        return _.isString(name) && name.startsWith(UI_PREFIX.CHECKBOX);
    },

    isControlSwitch: function(name){
        return _.isString(name) && name.startsWith(UI_PREFIX.CONTROL_SWITCH);
    },

    isEditBox: function(name){
        return _.isString(name) && name.startsWith(UI_PREFIX.EDIT_BOX);
    },

    isWebView: function(name){
        return _.isString(name) && name.startsWith(UI_PREFIX.WEB_VIEW);
    },

    isRickText: function(name){
        return _.isString(name) && name.startsWith(UI_PREFIX.RICH_TEXT);
    },

    isTableView: function(name){
        return _.isString(name) && name.startsWith(UI_PREFIX.TABLE_VIEW);
    },

    isHtmlText: function(name){
        return _.isString(name) && name.startsWith(UI_PREFIX.HTML_TEXT);
    },

    toControlSwitch: function(node){
        if(node.customData == null) {
            node.customData = "";
        }

        var arrData = node.customData.split("|");
        arrData.pop();
        if(arrData.length < UI_PARAMS_REQUIRE.CONTROL_SWITCH){
            if(arrData.length > 0){
                ZLog.error("toControlSwitch require " + UI_PARAMS_REQUIRE.CONTROL_SWITCH + " params for " + node.getName());
            }

            for(var i = arrData.length; i < UI_PARAMS_REQUIRE.CONTROL_SWITCH; ++i){
                arrData.push(this._controlSwitchDefault[i]);
            }
        }

        var mSwitch = new cc.ControlSwitch(
            new cc.Sprite(res[arrData[0]]),
            new cc.Sprite(res[arrData[1]]),
            new cc.Sprite(res[arrData[2]]),
            new cc.Sprite(res[arrData[3]]),
            new ccui.Text("", res[arrData[4]], res[arrData[5]]),
            new ccui.Text("", res[arrData[4]], res[arrData[5]])
        );
        mSwitch.setAnchorPoint(node.getAnchorPoint());
        mSwitch.setPosition(node.getPosition());

        return mSwitch;
    },

    toEditBox: function(textField){
        // TODO
    },

    toWebView: function(layer){
        // TODO
    },

    toRickText: function(layer){
        // TODO
    },

    toTableView: function(layer){
        // TODO
    },
}