/**
 * Created by bachbv on 1/16/2017.
 */

getSceneOfGUI = function(gui){
    if(gui == null) return null;

    var curParent = gui.getParent() != null ? gui.getParent() : null;
    while(curParent != null && !(curParent instanceof BaseScene)){
        curParent = curParent.getParent();
    }

    return curParent;
};

getListZOrder = function(node){
    if(node == null) return -1;

    var curNode = node;
    var list = [];

    while(!(curNode instanceof BaseScene) && curNode.getParent() != null){
        list.push(curNode.getLocalZOrder());
        curNode = curNode.getParent();
    }
    list.push(curNode);
    list.reverse();

    return list;
};

compareZOrder = function(node1, node2){
    var list1 = getListZOrder(node1);
    var list2 = getListZOrder(node2);

    if(list1.length > 0 && list2.length > 0 && list1[0] == list2[0]){
        var length = Math.min(list1.length, list2.length);

        for(var i = 1; i < length; ++i){
            if(list1[i] > list2[i]){
                // zOrder node1 > node2
                return 1;
            }
            else if(list1[i] == list2[i]){
                // continue;
            }
            else{
                // zOrder node1 < node2
                return -1;
            }
        }

        return 1;
    }
    else{
        ZLog.error("cannot compare zOrder of 2 nodes");
        return 0;
    }
};

var SceneMgr = cc.Class.extend({

    ctor:function () {
        this._currentScene = null;
        this._listScenes = {};
        this._sceneFactory = null;
        this._currentSceneId = -1;
        this._prevSceneId = -1;
        this._fog = null;
        this._fogListener = null;
        this._guiWaiting = null;
        return true;
    },

    /**
     *
     * @param {BaseScene} scene
     * @param {int} sceneId
     */
    addScene:function(scene, sceneId){
        this.removeScene(sceneId);
        this._listScenes[sceneId] = scene;
        scene.retain();
    },

    removeScene:function(sceneId) {
        if(this._listScenes[sceneId]){
            this._listScenes[sceneId].release();
            delete this._listScenes[sceneId];
        }
    },

    /**
     *
     * @param {int} sceneId
     * @returns {Layer}
     */
    getScene:function(sceneId)
    {
        if(this._listScenes[sceneId]) {
            return this._listScenes[sceneId];
        }
        else if(this._sceneFactory != null) {
            var screen = this._sceneFactory.createScreen(sceneId);
            if(screen != null) {
                this.addScene(screen,sceneId);
                return screen;
            }
        }

        ZLog.error("----> NOT FOUND scene id (%d)", sceneId);
        return null;
    },

    /**
     * get Layer by id in a scene
     * @param {int} layerId
     * @param {int} sceneId
     * @return {Layer} layer
     */
    getLayerInScene: function(layerId, sceneId){
        var scene = this.getScene(sceneId);

        if(scene){
            return scene.getLayer(layerId);
        }else{
            ZLog.error("----> NOT FOUND scene id (%d)", sceneId);
            return null;
        }
    },

    isExistScene: function(sceneId){
        return this._listScenes[sceneId];
    },

    /**
     *
     * @param {int} sceneId
     * @param isKeepPrevScene
     */
    viewSceneById:function(sceneId, isKeepPrevScene){
        if(isKeepPrevScene === undefined){
            isKeepPrevScene = true;
        }

        if (sceneId == this._currentSceneId) {
            return this._currentScene;
        }

        if(!isKeepPrevScene) {
            this.removeScene(sceneId);
        }

        this._prevSceneId = this._currentSceneId;
        this._currentSceneId = sceneId;
        this.viewScene(this.getScene(sceneId));

        return this._currentScene;
    },

    /**
     *
     * @param {cc.Layer} layer
     */
    viewScene:function(layer)
    {
        // check correct "Layer" type
        cc.arrayVerifyType(layer, cc.Layer);

        layer.removeFromParent(false);
        var scene = new cc.Scene();
        scene.addChild(layer);

        sceneMgr.cleanFogCache();

        // first scene showed without transition
        if(this._prevSceneId == -1){
            cc.director.runScene(scene);

        }else{
            var pTransition = this._sceneFactory.createTransition(scene);
            cc.director.runScene(pTransition);
        }
        this._currentScene = layer;
    },

    setSceneFactory:function(sceneFactory)
    {
        this._sceneFactory = sceneFactory;
    },

    /**
     *
     * @returns {number|*}
     */
    getPrevSceneId:function()
    {
        return this._prevSceneId;
    },

    /**
     *
     * @returns {number|*}
     */
    getCurrentSceneId:function()
    {
        return this._currentSceneId;
    },

    isPrevScene: function(sceneId){
        return this._prevSceneId == sceneId;
    },

    isScene: function(sceneId){
        return this._currentSceneId == sceneId;
    },

    /**
     *
     * @returns {BaseScene}
     */
    getCurrentScene:function()
    {
        return this._currentScene;
    },

    _createFog: function(){
        this._fog = new cc.LayerColor(cc.color(255,255,255), GV.VISIBALE_SIZE.width, GV.VISIBALE_SIZE.height);
        this._fog.retain();
        this._fog.setOpacity(180);
        this._fog.setVisible(false);

        this._fogListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();

                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);

                return cc.rectContainsPoint(rect, locationInNode);
            },
            onTouchMoved: function (touch, event) {
            },
            onTouchEnded: function (touch, event) {
            }
        });

        cc.eventManager.addListener(this._fogListener, this._fog);
    },

    /**
     *
     * @param target
     * @returns {number}
     */
    getIndexOfTarget: function(target){
        if(target == undefined || target == null || this.showFog._listGUI === undefined){
            return -1;
        }

        var listTarget = this.showFog._listGUI;
        for(var i = 0; i < listTarget.length; ++i){
            if(target == listTarget[i].target) return i;
        }

        return -1;
    },

    isShowFog: function(){
        return this._fog && this._fog.isVisible() && this._fog.getParent();
    },

    /**
     * showArrow the fog with opacity
     * target had added to parent
     * @param target
     * @param guiName
     * @param opacity
     * @returns {boolean}
     */
    showFog: function(target, guiName, opacity){
        if(opacity === undefined) opacity = 180;
        if(this.showFog._listGUI === undefined) this.showFog._listGUI = [];
        var zOrder = target.getLocalZOrder() - 1;

        if(this._currentScene){
            var fog = this.getFog();

            fog.setVisible(true);

            var index = this.getIndexOfTarget(target);
            if(index >= 0){
                // re-order target
                var parentObj = this.showFog._listGUI.splice(index, 1)[0];
                parentObj.zOrder = zOrder;

                fog.removeFromParent(false);
                target.getParent().addChild(fog, zOrder);
                this.showFog._listGUI.push(parentObj);
            }
            else{
                parentObj = {
                    target: target,
                    name: guiName,
                    zOrder: zOrder
                };

                var isHigherLayer = 1;
                if(this.showFog._listGUI.length > 0){
                    var lastObj = this.showFog._listGUI[this.showFog._listGUI.length - 1];
                    isHigherLayer = compareZOrder(target, lastObj.target);
                }

                if(isHigherLayer >= 0){
                    fog.removeFromParent(false);
                    target.getParent().addChild(fog, zOrder);

                    this.showFog._listGUI.push(parentObj);
                }
                else{
                    lastObj = this.showFog._listGUI.pop();
                    this.showFog._listGUI.push(parentObj);
                    this.showFog._listGUI.push(lastObj);
                }
            }

            if(opacity !== undefined){
                fog.setOpacity(opacity);
            }

            this._fogListener.setEnabled(true);
            return true;
        }

        return false;
    },

    _findFogOf: function(target){
        var list = this.showFog._listGUI;
        if(list && list.length > 0){
            for(var i = 0; i < list.length; ++i){
                if(target === list[i].target) return i;
            }
        }

        return -1;
    },

    _removeFogOf: function(target){
        var index = this._findFogOf(target);
        if(index >= 0){
            return this.showFog._listGUI.splice(index, 1)[0];
        }
        else{
            return null;
        }
    },

    hideFog: function(target){
        if(this.showFog._listGUI === undefined) this.showFog._listGUI = [];
        if(target == null || target === undefined) {
            ZLog.error("hideFog: no target found");
            return;
        }

        if(this.showFog._listGUI.length > 0){
            var fogOfTarget = this._removeFogOf(target);
            if(fogOfTarget){
                if(this.showFog._listGUI.length > 0){
                    var obj = this.showFog._listGUI[this.showFog._listGUI.length - 1];
                    var fog = this.getFog();

                    // re-addChild to other parent
                    fog.removeFromParent(false);
                    obj.target.getParent().addChild(fog, obj.zOrder);
                }
            }
            else{
                //ZLog.error("hideFog: no fogOfTarget found");
            }
        }

        if(this.showFog._listGUI.length == 0){
            this.getFog().setVisible(false);
            this._fogListener.setEnabled(false);
        }
    },

    cleanFogCache: function(){
        if(this.showFog._listGUI){
            this.showFog._listGUI.splice(0, this.showFog._listGUI.length);
        }

        if(this._fog) this._fog.setVisible(false);
    },

    /**
     * get the fog
     * @returns {cc.LayerColor}
     */
    getFog: function(){
        if(this._fog == null){
            this._createFog();
        }

        return this._fog;
    },

    showGUIWaiting: function(){
        //if(this._guiWaiting == null){
        //    this._guiWaiting = new GUIWaiting();
        //    this._guiWaiting.setVisible(false);
        //    this._guiWaiting.retain();
        //}
        //
        //this._guiWaiting.showAtCurrentScene();
    },

    hideGUIWaiting: function(){
        //if(this._guiWaiting){
        //    this._guiWaiting.hideArrow();
        //}
    },

    setVisibleLayersBehind: function(b){
        var currentScene = this.getCurrentScene();
        if(currentScene != null){
            //currentScene.getLayer(GV.LAYERS.BG).setVisible(b);
            //currentScene.getLayer(GV.LAYERS.GAME).setVisible(b);
            //currentScene.getLayer(GV.LAYERS.EFFECT).setVisible(b);
        }
    },
});