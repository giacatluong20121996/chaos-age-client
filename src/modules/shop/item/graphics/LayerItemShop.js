/**
 * Created by pc1 on 8/9/2018.
 */


var LayerItemShop = BaseGUI.extend({
    _nameClass: "LayerItemShop",
    _adapter: null,

    ctor: function(adapter){
        this._super();
        this._adapter = adapter;
        this.init();
    },

    init: function(){
        this._super();
        var rootNode = ccs.load(res.res_item_shop, "res/");
        this.addChild(rootNode.node);

        var listView = rootNode.node.getChildByName("list_view");
        listView.setDirection(ccui.ScrollView.DIR_HORIZONTAL);
        listView.setTouchEnabled(true);
        listView.setBounceEnabled(true);

        var customListView = new CustomListView(listView);
        customListView.setAdapter(this._adapter);

        listView.addEventListener(this.onClickItemListener.bind(this));

    },

    onClickItemListener: function(listView, eventEnum){
        if(eventEnum === ccui.ListView.ON_SELECTED_ITEM_START){
            var selectedItem = this._adapter.getListItems()[listView.getCurSelectedIndex()];
            selectedItem.onClick();
        }
        sceneMgr.viewSceneById(GV.SCENE_IDS.PLAY);
    },

    onTouchUIBeganEvent: function(sender){
        switch (sender){
            case this.btn_exit:
                this.getParent().removeFromParent();
                break;
        }
    }
});

