/**
 * Created by pc1 on 8/10/2018.
 */



var ResItemShop = ItemShop.extend({
    _className: "ResItemShop",
    _name: "name",
    _typeBuildingRes: null,
    _icon: "url",
    _timeBuilding: 0,
    _curNum: 0,
    _buyingRes: 0,
    _typeBuyingRes: 0,
    _iconBuyingRes: "url",

    _enable: true,


    ctor: function(name, idBuildingRes, idBuyingRes){
        this._name = name;
        this._typeBuildingRes = idBuildingRes;
        this._typeBuyingRes = idBuyingRes;
        this._icon = this.getIcon(idBuildingRes);
        this._timeBuilding = this.getTimeBuildingRes();
        this._curNum = this.getCurrentNumRes();
        this._buyingRes = this.getBuyingRes();
        this._iconBuyingRes = this.getIconBuyingRes(idBuyingRes);
    },

    getTimeBuildingRes: function(){
        //load config file
        return "1m0s";
    },

    getCurrentNumRes: function(){
        //load from MapMgr
        return "3/4";
    },

    getBuyingRes: function(){
        //load from ResMgr
        return 150;
    },

    getIcon: function(id){
        switch (id){
            case GV.BUILDING_RES_TYPE.GOLD:
                return res.item_shop_res_1;
            case GV.BUILDING_RES_TYPE.ELIXIR:
                return res.item_shop_res_2;
        }
    },

    getIconBuyingRes: function(id){
        switch (id){
            case GV.RES_TYPE.GOLD:
                return res.item_shop_icon_res_1;
            case GV.RES_TYPE.ELIXIR:
                return res.item_shop_icon_res_2;
        }
    },

    isEnable: function(){
        return this._enable;
    },

    onClick: function(){
        if(this._enable){
            cc.log("click");
            //do something
        }
    },

    onClickInfoButton: function(sender, type){
        cc.log("Show info here!");
    }
});

ResItemShop.createListItems = function(){
    var list = [];
    var item1 = new ResItemShop("Mỏ vàng", GV.BUILDING_RES_TYPE.GOLD, GV.RES_TYPE.ELIXIR);
    var item2 = new ResItemShop("Mỏ dầu", GV.BUILDING_RES_TYPE.ELIXIR, GV.RES_TYPE.GOLD);
    list.push(item1);
    list.push(item2);
    return list;
};
