/**
 * Created by pc1 on 8/9/2018.
 */

var CustomListView = cc.Class.extend({
    _className: "CustomListView",
    _base: null,
    _adapter: null,

    ctor: function(base){
        this._base = base;
    },

    setAdapter: function(adapter){
        this._adapter = adapter;
        this.loadItem();
    },

    loadItem: function(){
        for(var i = 0; i < this._adapter.getCount(); i++){
            var rootNode = ccs.load(this._adapter.getItemLayout(), "res/");
            var panel = rootNode.node.getChildByName(this._adapter.getPanelId());
            panel.removeFromParent(false);
            this._base.pushBackCustomItem(this._adapter.getItem(panel,i));
        }
    }
});
