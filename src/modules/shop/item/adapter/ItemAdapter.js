/**
 * Created by pc1 on 8/9/2018.
 */

var ItemAdapter = cc.Class.extend({
    _className: "ItemAdapter",
    _list : null,
    _itemLayout: null,
    _panelId: null,

    ctor: function(list, itemLayout, panelId){
        this._list = list;
        this._itemLayout = itemLayout;
        this._panelId = panelId;
    },

    getItemLayout: function(){
        return this._itemLayout;
    },

    getPanelId: function(){
        return this._panelId;
    },

    getItem: function(panel, position){
        return panel;
    },

    getCount: function(){
        return this._list.length;
    },

    getListItems: function(){
        return this._list;
    }
});