/**
 * Created by pc1 on 8/10/2018.
 */

var ResItemAdapter = ItemAdapter.extend({
   _className: "ResItemAdapter",

    ctor: function(list, itemLayout, panelId){
        this._super(list, itemLayout, panelId);
    },

    getItem: function(panel, position){
        var item = this._list[position];

        panel.getChildByName("name").setString(item._name);

        panel.getChildByName("icon").setTexture(item._icon);

        panel.getChildByName("timeBuilding").setString(item._timeBuilding);

        panel.getChildByName("curNum").setString(item._curNum);

        panel.getChildByName("buyingRes").setString(item._buyingRes);

        panel.getChildByName("iconBuyingRes").setTexture(item._iconBuyingRes);

        panel.getChildByName("btn_info").addTouchEventListener(item.onClickInfoButton, this);

        if(!item.isEnable())
            this.disableItem(panel);
        else
            this.enableItem(panel);

        return panel;
    },

    enableItem: function(node){
        for(var i = 0; i < node.getChildrenCount(); i++){
            if(node.children[i] instanceof cc.Sprite){
                node.children[i].setShaderProgram(shadersHelper.getShaders(ShadersHelper.TYPE.DEFAULT_SHADER));
            }
        }
    },

    disableItem: function(node){
        for(var i = 0; i < node.getChildrenCount(); i++){
            if(node.children[i] instanceof cc.Sprite){
                node.children[i].setShaderProgram(shadersHelper.getShaders(ShadersHelper.TYPE.GRAYSCALE_SHADERS));
            }
        }
    }
});





