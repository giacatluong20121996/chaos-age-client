/**
 * Created by pc1 on 8/10/2018.
 */

var ItemShop = cc.Class.extend({
    _className: "ItemShop",

    ctor: function(){

    },

    onClick: function(){
        //override
    },

    onClickInfoButton: function(sender, type){
        //override
    }
});


var ItemShopMgr = cc.Class.extend({
    _className: "ItemShopMgr",
    _itemsList: [],
    _factory: null,
    ctor: function(){

    },

    setItemShopFactory: function(factory){
        this._factory = factory;
    },

    getItemsList: function(id){
        if(this._itemsList[id] == null){
            this._itemsList[id] = this._factory.createListItems(id);
        }
        return this._itemsList[id]
    }
});

var ItemShopFactory = cc.Class.extend({
    createListItems: function(id){
        switch (id) {
            case GV.SHOP_CATEGORY.RES:
                return ResItemShop.createListItems();
        }
    }
});
