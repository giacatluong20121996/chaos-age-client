/**
 * Created by pc1 on 8/8/2018.
 */

/**
 * Created by pc1 on 8/8/2018.
 */

var LayerShop = BaseGUI.extend({
    _nameClass: "LayerInfo",

    ctor: function(){
        this._super();
        this.btn_res = null;
        this.btn_army = null;
        this.btn_exit = null;

        this.init();
    },

    init: function(){
        this._super();
        this.syncAllChildren(res.res_shop, this);
        this.setDeepSyncChildren(1);

        itemShopMgr = new ItemShopMgr();
        itemShopMgr.setItemShopFactory(new ItemShopFactory());
    },

    onTouchUIBeganEvent: function(sender){
        switch (sender){
            case this.btn_res:
                var resAdapter = new ResItemAdapter(
                    itemShopMgr.getItemsList(GV.SHOP_CATEGORY.RES),
                    "res/ui/ResItemShopLayout.json", "layout");
                sceneMgr.viewScene(new LayerItemShop(resAdapter));
                break;
            case this.btn_army:
                cc.log("army");
                break;
            case this.btn_exit:
                sceneMgr.viewSceneById(GV.SCENE_IDS.PLAY);
                break;
        }

    }
});

