/**
 * Created by VitaminB on 7/13/2015.
 */

var loginModule = null;
var loadingModule = null;

var ModuleMgr = cc.Class.extend({

    ctor: function(){
        this._nextId = 0;

        this._loginModule = new LoginModule(this.getNextModuleId());
        loginModule = this._loginModule;

        this._loadingModule = new LoadingModule(this.getNextModuleId());
        loadingModule = this._loadingModule;

        this._registerListener();
    },

    /**
     *
     * @private
     */
    _registerListener: function(){
        this._loginModule.registerListener();
        this._loadingModule.registerListener();
    },

    getLoginModule: function(){
        return this._loginModule;
    },

    getLoadingModule: function() {
        return this._loadingModule;
    },

    getNextModuleId: function(){
        return this._nextId++;
    },

    resetData: function(){

    },

    cleanUp: function(){
        for(var i in this){
            if(this[i] instanceof BaseModule){
                this[i].cleanUp && this[i].cleanUp();
            }
        }
    },
});