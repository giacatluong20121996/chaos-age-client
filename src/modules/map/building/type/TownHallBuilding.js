/**
 * Created by pc1 on 8/7/2018.
 */

var TownHallBuilding = Building.extend({
    _nameClass: "TownHallBuilding",
    _capacity: 0,
    _capacityGold: 0,
    _capacityElixir: 0,

    ctor: function(){
    },

    load: function(json){
        var self = this;
        cc.log("load");
        cc.loader.loadJson(res.TOW, function(error, data){
            for (var key in data[self.getBuildingId()][self.getLevel()]) {
                var value = data[self.getBuildingId()][self.getLevel()][key];
                switch (key) {
                    case "gold":
                        self.setGold(value);
                        break;
                    case "elixir":
                        self.setElixir(value);
                        break;
                    case "buildTime":
                        self.setBuildTime(value);
                        break;
                    case "capacity":
                        self.setCapacity(value);
                        break;
                    case "capacityGold":
                        self.setCapacityGold(value);
                        break;
                    case "capacityElixir":
                        self.setCapacityElixir(value);
                        break;
                    case "hitpoint":
                        self.setHitpoint(value);
                        break;
                    //case "townHallLevelRequired":
                    //    self.setTownHallLevelRequired(value);
                    //    break;
                    case "width":
                        self._wMap = value * 2;
                        break;
                    case "height":
                        self._hMap = value * 2;
                        break;
                }
            }
        });
        this._super(this._xMap, this.yMap, this.wMap, this.hMap);
    },

    setCapacity: function(capacity) {
        this._capacity = capacity;
    },

    getCapacity: function() {
        return this._capacity;
    },

    setCapacityGold: function(capacity) {
        this._capacityGold = capacity;
    },

    getCapacityGold: function() {
        return this._capacityGold;
    },

    setCapacityElixir: function(capacity) {
        this._capacityElixir = capacity;
    },

    getCapacityElixir: function() {
        return this._capacityElixir;
    },
});