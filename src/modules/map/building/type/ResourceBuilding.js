/**
 * Created by pc1 on 8/7/2018.
 */

var ResourceBuilding = Building.extend({
    _nameClass: "ResourceBuilding",
    _currentProduct: 0,

    ctor: function(id){
        this._super(id);
    },

    load: function(json){
        this._super(json);
        this.loadConfig(res.RES);
    }
});