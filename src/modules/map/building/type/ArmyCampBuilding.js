/**
 * Created by pc1 on 8/7/2018.
 */

var ArmyCampBuilding = Building.extend({
    _nameClass: "ArmyCampBuilding",
    _capacity: 0,

    ctor: function(){

    },

    load: function(json){
        var self = this;

        cc.loader.loadJson(res.AMC, function(error, data){
            for (var key in data[self.getBuildingId()][self.getLevel()]) {
                //cc.log(key + ": " + data[self.getBuildingId()][self.getLevel()][key]);
                var value = data[self.getBuildingId()][self.getLevel()][key];
                switch (key) {
                    case "elixir":
                        self.setElixir(value);
                        break;
                    case "buildTime":
                        self.setBuildTime(value);
                        break;
                    case "capacity":
                        self.setCapacity(value);
                        break;
                    case "hitpoint":
                        self.setHitpoint(value);
                        break;
                    case "townHallLevelRequired":
                        self.setTownHallLevelRequired(value);
                        break;
                    case "width":
                        self._wMap = value * 2;
                        break;
                    case "height":
                        self._hMap = value * 2;
                        break;
                }
            }
        });

        this._super(this._xMap, this.yMap, this.wMap, this.hMap);
    },

    setCapacity: function(capacity) {
        this._capacity = capacity;
    },

    getCapacity: function() {
        return this._capacity;
    },
});