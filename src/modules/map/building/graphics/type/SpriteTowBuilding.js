var SpriteTowBuilding = SpriteBuilding.extend({
    _className: "SpriteTowBuilding",
    _background: null,

    ctor: function(level){
        level = level || 1;
        this._super("res/Art/Buildings/townhall/TOW_1_" + level + "/idle/image0000.png");
    },

    init: function(){

    },

});
