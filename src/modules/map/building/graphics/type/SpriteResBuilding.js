var SpriteResBuilding = SpriteBuilding.extend({
    _className: "SpriteResBuilding",

    ctor: function(type, level){
        level = level || 1;
        switch (type) {
            case GV.OBJECT_IDS.RES_1:
                this._super("res/Art/Buildings/gold mine/RES_1_" + level + "/idle/image0000.png");
                var ani = new Animation();
                ani.attr = {
                    plist: res.res_1_1_plist,
                    img: res.res_1_1,
                    num: 10,
                    path: "Art/Effects/RES_1_1_effect/",
                    type: ".png",
                    duration: 0.2
                };
                ani.attach(this);
                break;
            case GV.OBJECT_IDS.RES_2:
                this._super("res/Art/Buildings/elixir collector/RES_2_" + level + "/idle/image0000.png");
                break;
        }
    }

});
