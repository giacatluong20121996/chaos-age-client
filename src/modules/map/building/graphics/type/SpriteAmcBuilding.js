var SpriteAmcBuilding = SpriteBuilding.extend({
    _className: "SpriteAmcBuilding",
    _background: null,

    ctor: function(level){
        level = level || 1;
        this._super("res/Art/Buildings/army camp/AMC_1_" + level + "/idle/image0000.png");
    },

    init: function(){

    },

});
