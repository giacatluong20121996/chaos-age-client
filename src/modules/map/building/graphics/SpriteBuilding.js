/**
 * Created by pc1 on 8/7/2018.
 */

var SpriteBuilding = SpriteObject.extend({
    _className: "SpriteBuilding",
    _arrow: null,

    ctor: function(texture){
        this._super(texture);

        //var sprite = cc.Sprite.create("Art/Effects/RES_1_1_effect/00.png");
        //cc.spriteFrameCache.addSpriteFrames(res.res_1_1_plist,res.res_1_1);
        //var animFrames = [];
        //var str = "";
        //var frame;
        //for (var i = 0; i < 10; i++) {
        //    str = "Art/Effects/RES_1_1_effect/" + (i < 10 ? ("0" + i) : i) + ".png";
        //    frame = cc.spriteFrameCache.getSpriteFrame(str);
        //    animFrames.push(frame);
        //}
        //var animation = new cc.Animation(animFrames, 0.2);
        //sprite.runAction(cc.animate(animation).repeatForever());
        //sprite.setPosition(this.x+this.width/2, this.y+this.height/2);
        //this.addChild(sprite);
        //
        //var to = cc.sequence(cc.progressTo(2, 100), cc.progressTo(0, 0));
        //
        //var left = new cc.ProgressTimer(new cc.Sprite("res/Art/GUIs/Main_Gui/g_bar.png"));
        //left.type = cc.ProgressTimer.TYPE_BAR;
        ////    Setup for a bar starting from the left since the midpoint is 0 for the x
        //left.midPoint = cc.p(0, 0);
        ////    Setup for a horizontal bar since the bar change rate is 0 for y meaning no vertical change
        //left.barChangeRate = cc.p(1, 0);
        //this.addChild(left);
        //left.runAction(to.repeatForever());

        this.initEvent();
    },

    initEvent: function(){
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan,
            onTouchMoved: this.onTouchMoved,
            onTouchEnded: this.onTouchEnded
        },this);
    },

    initGraph: function(){
        var bg = new Grass(this);
    },

    onTouchBegan: function(touch, event){
        var target = event.getCurrentTarget();
        var rect = target.getBoundingBox();
        //Check the click area
        if (cc.rectContainsPoint(rect, touch.getLocation())) {
            target.opacity = 180;
            target._arrow = movementBgMgr.setMovementBg(target);
            target._arrow.showArrow();
            return true;
        }else{
            if(target._arrow)
                target._arrow.hideArrow();
        }
        return false;
    },

    onTouchMoved: function(touch, event){
        var target = event.getCurrentTarget();
        var p = camera.calculate3DPosition(touch.getLocationX(), touch.getLocationY());

        if(Math.abs(p.x - target._objectMap._xMap) % 2 == 0) {
            target._objectMap._xMap = p.x;
            target.updatePosition();
        }
        if(Math.abs(p.y - target._objectMap._yMap) % 2 == 0) {
            target._objectMap._yMap = p.y;
            target.updatePosition();
        }
        target.updateGraph();
    },

    onTouchEnded: function(touch, event){
        var target = event.getCurrentTarget();
        target.opacity = 1000;
        target._arrow.hideMoveBg(false);
        //hide other layer info
    },

    updatePosition: function(){
        var p = camera.calculateScreenPosition(this._objectMap._xMap, this._objectMap._yMap);
        this.setPosition(p.x, p.y);
    },

    updateGraph: function(){
        this._arrow.showRedBg(mapMgr.checkConflictBoundary(this._objectMap));
    }
});
