/**
 * Created by pc1 on 8/7/2018.
 */

var Building = GameObject.extend({
    _nameClass: "Building",
    _state: 0,
    _startTime: 0,
    _level: 1,

    ctor: function(id){
        this._super(id);
    },


    load: function(json){
        this._super(json);
        this._level = json["level"] || 1;
        this._state = json["buildingState"];
        this._startTime = json["startTime"];
    },

    send: function(){

    },

    setLevel: function(level) {
        this._level = level || 1;
    },

    getLevel: function() {
        return this._level;
    }

});
