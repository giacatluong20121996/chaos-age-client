/**
 * Created by pc1 on 8/7/2018.
 */
var MapStatus = {
    MS_START: 0,
    MS_RUNNING: 1,
    MS_PAUSE: 2,
    MS_END: 3
};

var MapManager = cc.Class.extend({
    _nameClass: "CMapManager",
    _objectsMap: [],
    _maxWidth: 80,
    _maxHeight: 80,
    _width: 0,
    _height: 0,
    _status: null,
    _map: [],
    _jsonMap: {"RES_1":{"posX":24,"posY":20}, "RES_2":{"posX":50,"posY":50}},

    ctor: function(width, height){
        this._width = width;
        this._height = height;
        this._status = MapStatus.MS_START;

        this._map = new Array(this._maxWidth);
        for(var i = 0; i < this._maxWidth; i++){
            this._map[i] = new Array(this._maxHeight);
            for(var j = 0; j < this._maxHeight; j++){
                this._map[i][j] = 1;
            }
        }
        //this.load(this._jsonMap);
    },

    getOjectsMap: function(index){
        return this._objectsMap[index];
    },

    load: function(json){
        for(var key in json){
            cc.log(key);
            var obj = null;
            switch (key){
                case GV.OBJECT_IDS.RES_1:
                    obj = new ResourceBuilding(GV.OBJECT_IDS.RES_1);
                    break;
                case GV.OBJECT_IDS.RES_2:
                    obj = new ResourceBuilding(GV.OBJECT_IDS.RES_2);
                    break;
                //case GV.OBJECT_IDS.TOW_1:
                //    obj = new TownHallBuilding(GV.OBJECT_IDS.TOW_1);
                //    break;
                //case GV.OBJECT_IDS.AMC_1:
                //    obj = new ArmyCampBuilding(GV.OBJECT_IDS.AMC_1);
                //    break;
            }
            if(obj) {
                obj.load(json[key]);
                this.add(obj);
            }
        }

        layerMap.show();
    },

    add: function(object){
        if(object != null && object instanceof GameObject) {
            this._objectsMap.push(object);
            for (var i = object._xMap - object._wMap / 2 + 1; i < object._xMap + object._wMap / 2 + 1; i++) {
                for (var j = object._yMap - object._hMap / 2 + 1; j < object._yMap + object._hMap / 2 + 1; j++) {
                    this._map[i][j] = 0;
                }
            }
        }
        else{
            cc.log("Cannot add [object] to [Map]");
        }
        //for(var k = 0 ; k < 80; k++){
        //    cc.log(this._map[k]);
        //}
    },

    remove: function(object){
        for(var i = 0; i < this._objectsMap.length; i++){
            if(object === this._objectsMap[i]){
                this._objectsMap.splice(i,1);
            }
        }
    },

    getNumOfObjs: function(){
        return this._objectsMap.length;
    },

    checkConflictBoundary: function(object){
        for(var i = 0; i < this._objectsMap.length; i++){
            if(object === this._objectsMap[i]) continue;
            if(cc.rectIntersectsRect(object.getRect(), this._objectsMap[i].getRect())){
                return true;
            }
        }
        return false;
    }
});


