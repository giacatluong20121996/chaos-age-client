/**
 * Created by pc1 on 8/8/2018.
 */

var LayerInfo = BaseGUI.extend({
    _nameClass: "LayerInfo",
    _layerShop: null,

    ctor: function(resLayer){
        this._super();
        this.btn_shop = null;

        this.init(resLayer);
    },

    init: function(resLayer){
        this._super();
        this.syncAllChildren(resLayer, this);
        this.setDeepSyncChildren(1);
    },


    onTouchUIBeganEvent: function(sender){
        switch (sender){
            case this.btn_shop:
                sceneMgr.viewSceneById(GV.SCENE_IDS.SHOP);
                break
        }

    }
});
