/**
 * Created by pc1 on 8/8/2018.
 */

var PlayScene = BaseScene.extend({
   _className: "PlayScene",

    ctor: function(){
        this._super();

        this.init();
    },

    init: function(){
        this._super();

        layerMap = new LayerMap(res.res_map);
        var layerInfo = new LayerInfo(res.res_info);
        this.addChild(layerMap);
        this.addChild(layerInfo);

        // loadingModule.sendLoadingSignal(1);

        shadersHelper = new ShadersHelper();
    }
});
