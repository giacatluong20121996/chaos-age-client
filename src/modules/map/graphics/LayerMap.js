/**
 * Created by pc1 on 8/7/2018.
 */

var num = 80;
var scale = 1;

var LayerMap = BaseGUI.extend({
    _className: "LayerMap",
    _vx: 0,
    _vy: 0,
    _ax: 0,
    _ay: 0,
    _desX: 0,
    _desY: 0,
    _startX: 0,
    _startY: 0,
    _isMoving: false,

    ctor: function(resMap){
        this._super();
        this.init(resMap);
    },
    init: function(resMap){
        this._super();

        this.syncAllChildren(resMap, this);
        this.setDeepSyncChildren();

        mapMgr = new MapManager(80,80);
        camera = new Camera();

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan,
            onTouchMoved: this.onTouchMoved,
            onTouchEnded: this.onTouchEnded
        }, this);

        //this.drawGrid();
    },

    onTouchBegan: function(touch, event){
        var target = event.getCurrentTarget();
        target._startX = touch.getLocationX();
        target._startY = touch.getLocationY();
        //zoom test
        //scale += 0.01;
        //target.zoom(touch.getLocationX(), touch.getLocationY(), scale);
        return true;
    },

    onTouchMoved: function(touch, event){
        var target = event.getCurrentTarget();
        if(!target._isMoving){
            target.scheduleUpdate();
            target._isMoving = true;
        }
        var location = touch.getLocation();
        target._desX = location.x;
        target._desY = location.y;

        var delX = location.x - target._startX;
        var delY = location.y - target._startY;

        var h = Math.cos(Math.atan(delX/delY))*(delY > 0 ? 1 : -1);
        target._vy = 500 * h;
        target._vx = Math.sqrt(500*500 - target._vy*target._vy)*(delX > 0 ? 1 : -1);
        target._ay = - 300 * h;
        target._ax = - Math.sqrt(300*300 - target._ay*target._ay)*(delX > 0 ? 1 : -1);
    },

    onTouchEnded: function(touch, event){
    },

    zoom: function(anchorX, anchorY, scale){
        if(scale > 1.5) return;

        var matrix = camera.getZoomAnchorPointMatrix(anchorX,anchorY,scale);
        if(matrix == null) return;
        camera.zoom(anchorX,anchorY,scale, matrix);
        var chils = this.getChildren();
        for(var i = 0; i < chils.length; i++){
            var p = Matrix.Multify(matrix, [[chils[i].x], [chils[i].y], [1]]);
            chils[i].setPosition(p[0][0], p[1][0]);
            chils[i].setScale(chils[i].getScale() * scale);
        }
    },

    update: function(dt){
        if(Math.abs(this._startX - this._desX) < 10 && Math.abs(this._startY - this._desY) < 10){
            this._vx = 0;
            this._vy = 0;
        }
        if(camera.outOfMap(this._vx, this._vy)){
            this._vx = 0;
            this._vy = 0;
        }
        if(Math.abs(this._vx) < 10 && Math.abs(this._vy) < 10) {
            this._ax = 0;
            this._ay = 0;
            this.unscheduleUpdate();
            this._isMoving = false;
            return;
        }
        this._vx += this._ax*dt;
        this._vy += this._ay*dt;
        this._startX += this._vx*dt;
        this._startY += this._vy*dt;

        //updateCamera camera
        camera.move(-this._vx*dt, -this._vy*dt);

        //updateCamera Graphics
        this.updateGraph(this._vx*dt, this._vy*dt);
    },

    updateGraph: function(dx, dy){
        var chils = this.getChildren();
        for(var i = 0; i < chils.length; i++){
            chils[i].x += dx;
            chils[i].y += dy;
            if(chils[i] instanceof NPC){
                chils[i]._desX += dx;
                chils[i]._desY += dy;
            }
        }
    },

    //drawGrid: function(){
    //    var x = [[0,0,1]];
    //    var y = [[0,0,1]];
    //    for(var i = 0; i <= num; i+=2){
    //        x[0][1] = i;
    //        y[0][0] = num;
    //        y[0][1] = i;
    //        var xp = Matrix.Multify(camera.matrix, Matrix.T(x));
    //        var yp =Matrix. Multify(camera.matrix, Matrix.T(y));
    //        var drawNode = new cc.DrawNode();
    //        drawNode.drawSegment(cc.p(xp[0][0],xp[1][0]), cc.p(yp[0][0],yp[1][0]),1,cc.Color(250,0,0,255));
    //        this.addChild(drawNode);
    //    }
    //
    //    var x = [[0,0,1]];
    //    var y = [[0,0,1]];
    //    for(var i = 0; i <= num; i+=2){
    //        x[0][0] = i;
    //        y[0][1] = num;
    //        y[0][0] = i;
    //        var xp = Matrix.Multify(camera.matrix, Matrix.T(x));
    //        var yp = Matrix.Multify(camera.matrix, Matrix.T(y));
    //        var drawNode = new cc.DrawNode();
    //        drawNode.drawSegment(cc.p(xp[0][0],xp[1][0]), cc.p(yp[0][0],yp[1][0]),1,cc.Color(250,0,0,255));
    //        this.addChild(drawNode);
    //    }
    //},

    show: function(){
        for(var i = 0; i < mapMgr.getNumOfObjs(); i++){
            var sprite = null;
            switch (mapMgr.getOjectsMap(i).getId()){
                case GV.OBJECT_IDS.RES_1:
                    sprite = new SpriteResBuilding(GV.OBJECT_IDS.RES_1, mapMgr.getOjectsMap(i).getLevel());
                    break;
                case GV.OBJECT_IDS.RES_2:
                    sprite = new SpriteResBuilding(GV.OBJECT_IDS.RES_2, mapMgr.getOjectsMap(i).getLevel());
                    break;
                //case GV.OBJECT_IDS.TOW_1:
                //    sprite = new SpriteTowBuilding();
                //    break;
                //case GV.OBJECT_IDS.AMC_1:
                //    sprite = new SpriteAmcBuilding();
                //    break;
            }
            if(sprite){
                sprite.setScale(0.5);
                sprite.setObjectMap(mapMgr.getOjectsMap(i));
                sprite.setLocalZOrder(0);
                this.addChild(sprite);
                sprite.initGraph();
            }
        }

        //var npc = new NPC("res/texture/ball.png");
        //npc.setLocalZOrder(2);
        //this.addChild(npc);
        //var graph = new Graph(mapMgr._map, {diagonal: true});
        //npc.move(graph ,new GridNode(50,50,1), new GridNode(30,20,1));
    }
});
