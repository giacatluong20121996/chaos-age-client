/**
 * Created by pc1 on 8/7/2018.
 */

var SpriteObject = cc.Sprite.extend({
    _nameClass: "SpriteObject",
    _objectMap: null,
    _isShowing: false,
    _attr: null,

    ctor: function(texture){
        this._super(texture);

        movementBgMgr = new MovementBgMgr();
    },

    setObjectMap: function(object){
        this._objectMap = object;
        var p = camera.calculateScreenPosition(this._objectMap._xMap, this._objectMap._yMap);
        this.setPosition(p.x, p.y);
    },

});
