/**
 * Created by pc1 on 8/13/2018.
 */
var MovementBg = cc.Sprite.extend({
    _nameClass: "MovementBg",
    _green: null,
    _red: null,
    _object: null,
    _isAttach: false,
    _isShow: false,

    ctor: function(){
        this._super();
    },

    showArrow: function(){
        var scaleAction = cc.ScaleTo.create(0.1,2,2);
        this.runAction(scaleAction);
        this.setVisible(true);
        this._isShow = true;
        movementBgMgr.hideOther(this);
    },

    hideArrow: function(){
        var scaleAction = cc.ScaleTo.create(0.3,1,1);
        this.runAction(scaleAction);
        this.scheduleOnce(this.endHideAni, 0.3);
    },
    endHideAni: function(){
        this.setVisible(false);
        this._isShow = false;
    },

    showRedBg: function(show){
        this._green.setVisible(!show);
        this._red.setVisible(show);
    },

    hideMoveBg: function(){
        this._green.setVisible(false);
        this._red.setVisible(false);
    },

    attach: function(object){
        this._object = object;
        if(!this._isAttach) {
            var textureArrow = null, textureGreen = null, textureRed = null;
            switch (object._objectMap.getId()){
                case GV.OBJECT_IDS.TOW_1:
                    textureArrow = res.arrow + "1.png";
                    break;
                case GV.OBJECT_IDS.RES_1:
                    textureArrow = res.arrow + "2.png";
                    textureGreen = res.bg_green_move + "4.png";
                    textureRed = res.bg_red_move + "4.png";
                    break;
                case GV.OBJECT_IDS.RES_2:
                    textureArrow = res.arrow + "2.png";
                    textureGreen = res.bg_green_move + "4.png";
                    textureRed = res.bg_red_move + "4.png";
                    break;
            }
            if(textureArrow && textureGreen && textureRed) {
                this.setTexture(textureArrow);
                this.setPosition(object.width/2, object.height/2);
                this.setLocalZOrder(object.getLocalZOrder() - 1);
                this.setVisible(false);

                this._green = cc.Sprite.create(textureGreen);
                this._green.setPosition(this.width/2, this.height/2);
                this._green.setVisible(false);

                this._red = cc.Sprite.create(textureRed);
                this._red.setPosition(this.width/2, this.height/2);
                this._red.setVisible(false);

                this.addChild(this._green);
                this.addChild(this._red);
            }
            object.addChild(this);
            this._isAttach = true;
        }
    },

    detach: function(){
        if(this._isAttach) {
            this.removeFromParent(true);
            this._isAttach = false;
        }
    }
});

var MovementBgMgr = cc.Class.extend({
    _className: "MovementBgMgr",
    _list: [],
    setMovementBg: function(object){
        if(object._arrow) return object._arrow;
        for(var i = 0; i < this._list.length; i++){
            if(this._list[i] && !this._list[i]._isShow){
                this._list[i].detach();
                this._list[i].attach(object);
                return this._list[i];
            }
        }
        var mbg = new MovementBg();
        mbg.attach(object);
        this._list.push(mbg);
        return mbg;
    },

    hideOther: function(mbg){
        for(var i = 0; i < this._list.length; i++){
            if(this._list[i] !== mbg){
                this._list[i].hideArrow();
            }
        }
    }
});

