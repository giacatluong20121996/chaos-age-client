/**
 * Created by Minh Nhi on 8/13/2018.
 */

var Animation = cc.Class.extend({
    _className: "Animation",
    attr: {},
    ctor: function(){
    },

    attach: function(object){
        var sprite = cc.Sprite.create(this.attr.path + "00" + this.attr.type);
        cc.spriteFrameCache.addSpriteFrames(this.attr.plist,this.attr.img);
        var animFrames = [];
        var str = "";
        var frame;
        for (var i = 0; i < this.attr.num; i++) {
            str = this.attr.path + (i < 10 ? ("0" + i) : i) + this.attr.type;
            frame = cc.spriteFrameCache.getSpriteFrame(str);
            animFrames.push(frame);
        }
        var animation = new cc.Animation(animFrames, 0.2);
        sprite.runAction(cc.animate(animation).repeatForever());
        sprite.setPosition(object.x+object.width/2, object.y+object.height/2);
        object.addChild(sprite);
    }
});
