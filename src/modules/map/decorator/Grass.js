/**
 * Created by pc1 on 8/13/2018.
 */
var Grass = cc.Sprite.extend({
    _nameClass: "Grass",
    _object: null,

    ctor: function(object){
        this._super();
        this._object = object;

        var texture = null;
        switch (object._objectMap.getId()){
            case GV.OBJECT_IDS.TOW_1:
                texture = res.grass + "4.png";
                break;
            case GV.OBJECT_IDS.RES_1:
                texture = res.grass + "4.png";
                break;
            case GV.OBJECT_IDS.RES_2:
                texture = res.grass + "4.png";
                break;

        }
        if(texture) {
            this.setTexture(texture);
            this.setScale(2);
            this.setPosition(object.width/2, object.height/2);
            this.setLocalZOrder(object.getLocalZOrder() - 1);
            object.addChild(this);
        }
    },

});