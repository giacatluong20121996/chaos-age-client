/**
 * Created by pc1 on 8/7/2018.
 */

var GameObject = cc.Class.extend({
    _nameClass: "GameObject",
    _xMap: 0,
    _yMap: 0,
    _wMap: 0,
    _hMap: 0,
    _attr: null,
    _id: null,

    ctor: function(id){
        this._id = id;
        this.init();
    },


    init: function(){
    },

    load: function(json){
        this._xMap = json["posX"];
        this._yMap = json["posY"];
    },

    loadConfig: function(configRes){
        var self = this;
        cc.loader.loadJson(configRes,function(error, data){
            self._attr = data[self.getId()][self.getLevel()];
        });
        this._wMap = this._attr["width"]*2;
        this._hMap = this._attr["height"]*2;
    },

    getId: function(){
        return this._id;
    },

    setMapPosition: function(xMap, yMap){
        this._xMap = xMap;
        this._yMap = yMap;
    },

    getMapPosition: function(){
        return cc.p(this._xMap, this._yMap);
    },

    getRect: function(){
        return cc.rect(this._xMap, this._yMap, this._wMap, this._hMap);
    },


});
