/**
 * Created by pc1 on 8/7/2018.
 */

var Camera = cc.Class.extend({
    _className: "Camera",
    x: 0,
    y: 0,
    w: 960,
    h: 640,
    matrix: null,

    ctor: function(){
        this.matrix = Matrix.Multify(this.getTransformMatrix(318,278.5, 1),Matrix.isoMap);
        this.updateCamera();
    },

    move: function(delx, dely){
        this.matrix = Matrix.Multify(this.getMoveMatrix(delx, dely), this.matrix);
        this.updateCamera();
    },

    zoom: function(anchorX, anchorY, scale, m ){
        this.matrix = Matrix.Multify(m ,this.matrix);
        this.updateCamera();
    },

    updateCamera: function(){
        var matrix = Matrix.Multify(Matrix.isoMap, Matrix.Inverse(this.matrix));
        var p = Matrix.Multify(matrix, [[0],[0],[1]]);
        this.x = p[0][0];
        this.y = p[1][0];
        p = Matrix.Multify(matrix, [[cc.winSize.width], [cc.winSize.height], [1]]);
        this.w = p[0][0] - this.x;
        this.h = p[1][0] - this.y;
    },

    outOfMap: function(vx, vy){
        return ((this.x < 0 &&  vx > 0)
        || (this.y < 0 && vy > 0)
        || (this.x + this.w > 1700 && vx < 0)
        || (this.y + this.h > 1300 && vy < 0));
    },

    getZoomAnchorPointMatrix: function(aX, aY, scale){
        var m1 = [[scale,0,aX*(1 - scale)], [0,scale,aY*(1 - scale)], [0,0,1]];
        if (scale > 1)  return m1;

        var d1, d2, d3, d4;
        d1 = this.x + this.w - 1700;
        d2 = this.x;
        d3 = this.y + this.h -1300;
        d4 = this.y;

        var dx = 1, dy = 1;

        if(d1 > 0 && d2 >= 0) dx = d1;
        else if(d2 < 0 && d1 <= 0) dx = d2;
        else if(d1 > 0 && d2 < 0) return null;
        else dx = 0;

        if(d3 > 0 && d4 >= 0) dy = d3;
        else if(d4 < 0 && d3 <= 0) dy = d4;
        else if(d3 > 0 && d4 < 0) return null;
        else dy = 0;

        var m2 = [[1,0,dx],[0,1,dy],[0,0,1]];
        return Matrix.Multify(m2,m1);
    },

    getMoveMatrix: function(x, y){
        return [[1,0,-x],[0,1,-y],[0,0,1]];
    },

    getZoomMatrix: function(scale){
        scale = scale || 1;
        return [[scale,0,0],[0,scale,0],[0,0,1]];
    },


    getTransformMatrix: function(x,y,scale){
        scale = scale || 1;
        return [[scale,0,-scale*x],[0,scale,-scale*y],[0,0,1]];
    },

    calculateScreenPosition: function(xMap, yMap){
        var p = [[xMap],[yMap], [1]];
        p = Matrix.Multify(this.matrix, p);
        return {x: p[0][0], y: p[1][0]};
    },

    calculateMapPosition: function(xS, yS){
        var matrix = Matrix.Multify(Matrix.isoMap, Matrix.Inverse(this.matrix));
        var p = Matrix.Multify(matrix, [[xS],[yS],[1]]);
        return {x: p[0][0], y: p[1][0]};
    },

    calculate3DPosition: function(xS, yS){
        var matrix = Matrix.Inverse(this.matrix);
        var p = [[xS], [yS], [1]];
        p = Matrix.Multify(matrix, p);
        return {x: Math.round(p[0][0]), y: Math.round(p[1][0])};
    }

});

