/**
 * Created by pc1 on 8/7/2018.
 */

var Matrix = {
    //isoMap: [[9.268749999828351,-9.268749999828351,797.0],
    //    [6.943987087012403,6.943987087012403,28.0],
    //    [0,0,1]],

    isoMap: [[9.52,-9.52,797.0],
            [7.13,7.13,28.0],
            [0,0,1]],

    T: function(x){
        var rows = x.length, cols = x[0].length,
            m = new Array(cols);
        for(var r = 0; r < cols; r++){
            m[r] = new Array(rows);
            for(var c = 0; c < rows; c++){
                m[r][c] = x[c][r];
            }
        }
        return m;
    },

    Multify: function(a, b){
        var aNumRows = a.length, aNumCols = a[0].length, bNumCols = b[0].length,
            m = new Array(aNumRows);
        for (var r = 0; r < aNumRows; ++r) {
            m[r] = new Array(bNumCols);
            for (var c = 0; c < bNumCols; ++c) {
                m[r][c] = 0;
                for (var i = 0; i < aNumCols; ++i) {
                    m[r][c] += a[r][i] * b[i][c];
                }
            }
        }
        return m;
    },

    Reduction:  function(a, size, pivot, col) {
        var i, j;
        var factor;
        factor = a[pivot][col];

        for (i = 0; i < 2 * size; i++) {
            a[pivot][i] /= factor;
        }
        for (i = 0; i < size; i++) {
            if (i != pivot) {
                factor = a[i][col];
                for (j = 0; j < 2 * size; j++) {
                    a[i][j] = a[i][j] - a[pivot][j] * factor;
                }
            }
        }
    },

    Inverse: function(matrix){
        matrix = this.T(matrix.concat([[1,0,0],[0,1,0],[0,0,1]]));
        for (var i = 0; i < 3; i++) {
            this.Reduction(matrix, 3, i, i);
        }
        return this.T(matrix).slice(3,6);
    }

};
