/**
 * Created by Minh Nhi on 8/11/2018.
 */

var NPC = cc.Sprite.extend({
    _nameClass : "NPC",
    _path: null,
    _vx: 0,
    _vy: 0,
    _desX: 0,
    _desY: 0,

    ctor: function(texture){
        this._super(texture);
    },


    move: function(graph, start, goal){
        this._path = astar.search(graph, start, goal, {closest: true, heuristic: astar.heuristics.diagonal});
        var start = this._path.shift();
        var p = [[start.x],[start.y], [1]];
        p = Matrix.Multify(camera.matrix, p);
        this.setPosition(p[0][0],p[1][0]);
        this.next();
        this.scheduleUpdate();
    },

    next: function(){
        var gold = this._path.shift();
        if(gold == null){
            this.unscheduleUpdate();
            return;
        }
        var p = [[gold.x],[gold.y], [1]];
        p = Matrix.Multify(camera.matrix, p);
        this._desX = p[0][0];
        this._desY = p[1][0];

        var delX = this._desX - this.x;
        var delY = this._desY - this.y;
        var h = Math.cos(Math.atan(delX/delY))*(delY > 0 ? 1 : -1);
        this._vy = 50 * h;
        this._vx = Math.sqrt(50*50 - this._vy*this._vy)*(delX > 0 ? 1 : -1);
    },

    update: function(dt){
        if(Math.abs(this._desX - this.x) < 3 && Math.abs(this._desY - this.y) < 3){
            this.next();
        }
        this.x += this._vx*dt;
        this.y += this._vy*dt;
    }
});

