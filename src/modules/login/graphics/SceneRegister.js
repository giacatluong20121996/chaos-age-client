/**
 * Created by bachbv on 11/24/2015.
 */

var SceneRegister = BaseScene.extend({
    _className: "SceneRegister",

    ctor: function(){
        this._super();

        this.btnBack = null;
        this.btnRegister = null;
        this.txtUsername = null;
        this.txtPassword = null;
        this.txtRePassword = null;
        this.lbRegisterFail = null;
        this.lbRegisterFailNote = null;
        this.init();
    },

    init: function(){
        this._super();
        this.syncAllChildren("res/ui/SceneRegister.json", this);
        this.lbRegisterFail.setVisible(false);
        this.lbRegisterFailNote.setVisible(false);
    },

    onExit: function(){
        this._super();
    },

    onTouchUIEndEvent: function (sender) {
        //Audio.playEffect(res.button_click);

        switch (sender) {
            case this.btnRegister:
                var username = this.txtUsername.getString();
                var password = this.txtPassword.getString();
                var rePassword = this.txtRePassword.getString();
                if (!this.checkValidData(username) || !this.checkValidData(password) || !this.checkValidData(rePassword)) {
                    this.showLabel("Please enter your' username or password.");
                    this.showLabelNote("Ex: Can't empty, Length < 10, Only use: word, digit, '-'");
                    break;
                } else if (password != rePassword){
                    cc.log(username);
                    this.showLabel("Password does not match the confirm password.");
                    break;
                } else {
                    if(GV.MODE == BUILD_MODE.DEV){
                        // in dev mode, user name is session key

                        moduleMgr.getLoginModule().setSessionKey(username);
                        moduleMgr.getLoginModule().setUserData(username, password);
                        moduleMgr.getLoginModule().setLoginType(GV.TYPE_LOGIN.REGISTER);
                        cc.log(this.txtUsername.getString());
                        connector.connect();
                        //this.showLabel();
                    }
                    else{
                        // TODO check uName and uPassword, then ...
                    }
                }
                break;
            case this.btnBack:
                sceneMgr.removeScene(GV.SCENE_IDS.REGISTER);
                sceneMgr.viewSceneById(GV.SCENE_IDS.LOGIN);
                break;
        }
    },

    showLabel: function(str){
        if (this.lbRegisterFail.isVisible() == false) {
            this.lbRegisterFail.setVisible(true);
        }
        this.lbRegisterFail.setString(str);
        //this.lbRegisterFailNote.runAction(cc.sequence(cc.fadeOut(1.0), cc.delayTime(0.25), fade.reverse(), cc.delayTime(0.25)));
    },

    showLabelNote: function(str){
        if (this.lbRegisterFailNote.isVisible() == false) {
            this.lbRegisterFailNote.setVisible(true);
        }
        this.lbRegisterFailNote.setString(str);
    },

    checkValidData: function (str) {
        if (str == "") {
            return false;
        } else if (str.length > 10) {
            return false;
        } else if (/[^a-zA-Z0-9\-\/]/.test(str)) {
            return false;
        }
        return true;
    },
});
