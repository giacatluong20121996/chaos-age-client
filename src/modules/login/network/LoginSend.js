/**
 * Created by bachbv on 12/23/2015.
 */

var CmdSendHandshake = fr.OutPacket.extend({
    ctor: function () {
        this._super();
        this.initData(2);
        this.setControllerId(0);
        this.setCmdId(CMD.HAND_SHAKE);
    },
});

CmdSendLogin = fr.OutPacket.extend({
    ctor: function (data) {
        this._super();
        this.initData(2);
        this.setCmdId(CMD.USER_LOGIN);

        this.sessionKey = data.sessionKey;
        this.userData = data.userData;
        this.loginType = data.loginType;

    },

    putData:function(){
        this.putString(this.sessionKey);
        this.putString(JSON.stringify(this.userData));
        this.putInt(this.loginType);
    }
});

