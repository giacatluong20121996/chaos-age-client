/**
 * Created by bachbv on 1/21/2017.
 */

CmdReceiveLogin = BaseInPacket.extend({
    ctor:function() {
        this._super();
        this.recieveMsg = "";
        this.config = "";
    },

    readData:function(){
        this.recieveMsg = this.getBool();
        this.config = this.getString();
    }
});

CmdReceiveDisconnect = BaseInPacket.extend({
    ctor:function() {
        this._super();
    }
});

CmdReceiveHandShake = BaseInPacket.extend({
    ctor:function() {
        this._super();
        this._sessionToken = "";
    },

    readData:function(){
        this._sessionToken = this.getString();
    }
});