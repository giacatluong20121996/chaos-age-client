/**
 * Created by DELL on 8/10/2018.
 */
var CmdLoadingAllInfo = BaseInPacket.extend({

    ctor: function () {
        this.config = "";
        this._super();
    },

    readData: function () {
        var map = this.getString();
        var player = this.getString();
        var obs = this.getString();

        this.config = map + player + obs;
    }
});
