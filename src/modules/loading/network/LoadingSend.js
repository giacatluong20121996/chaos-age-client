/**
 * Created by DELL on 8/10/2018.
 */

var CmdSendLoadingSignal = BaseOutPacket.extend({
    ctor: function (_idUser) {
        this._super();
        this.initData(2);
        this.setCmdId(CMD.LOADING_ALL_INFO);
        this.idUser = _idUser;
    },

    putData: function() {
        this.putString(this.idUser);
    }
});


