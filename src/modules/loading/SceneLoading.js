/**
 * Created by VitaminB on 7/13/2015.
 */
var SceneLoading = BaseScene.extend({
    _className: "SceneLoading",

    ctor: function(){
        this._super();

        // init variables
        this.lbPercent = null;
        this.lbVersion = null;
        this._numberOfSprites = 0;
        this._numberOfLoadedSprites = 0;
        this._imgProgressBar = null;
        this.imgBgProgress = null;

        this.init();
    },

    init: function(){
        this._super();
        var layerBg = this.getLayer(GV.LAYERS.BG);
        this.syncAllChildren(res.scene_loading, layerBg);
        this.doLayout(GV.VISIBALE_SIZE);

        this.lbVersion.setVisible(true);
        this.setVersion(GV.VERSION_NAME);

        this.lbPercent.setString("");
        this.lbPercent.removeFromParent(false);

        this._imgProgressBar = new ccui.LoadingBar(res.progress_bar, 0);
        this._imgProgressBar.setPosition(this.imgBgProgress.x, this.imgBgProgress.y);

        layerBg.addChild(this._imgProgressBar);
        layerBg.addChild(this.lbPercent);
    },

    setVersion: function(version){
        this.lbVersion.setString(GV.MODE + " | v" + version);
    },

    /**
     *load texture array by String
     * @param {Array} textureArr
     */
    loadTextures: function(textureArr){
        this._numberOfSprites += textureArr.length;

        if(!cc.sys.isNative) {
            for(var i = 0; i < textureArr.length; ++i){
                cc.loader.load(textureArr[i], function(err, res) {
                    if(!err) {
                        this.loadingCallBack();
                    }
                    else
                        ZLog.error("can not load %s", res);
                }.bind(this));
            }
        }
        else {
            var texCache = cc.textureCache;
            for(i = 0; i < textureArr.length; ++i){
                texCache.addImageAsync(textureArr[i], this.loadingCallBack, this);
            }
        }
    },

    loadPlistFiles: function(){
        resourceMgr.loadPlist(res.pack_gold_plist);
        resourceMgr.loadPlist(res.pack_main_lobby_ui_plist);
        resourceMgr.loadPlist(res.pack_main_table_ui_plist);
        resourceMgr.loadPlist(res.avatar_borders_plist);
        resourceMgr.loadPlist(res.cards_chips_plist);
        resourceMgr.loadPlist(res.emoticons_plist);
        resourceMgr.loadPlist(res.vip_texture_plist);
        resourceMgr.loadPlist(res.tournament_rings_plist);
    },

    loadAnimations: function(animation_list) {
        for(var i=0; i<animation_list.length; i++)
            cc.loader.load(animation_list[i], function(err, res) {
                if(err)
                    cc.log("error to load %s", res);
            });
    },

    loadDBAnimations: function(){
        var list = [
            res.anim_btn_quick_play,
            res.anim_btn_tournament,
            res.anim_btn_pick_table,
            res.anim_btn_lobby_add_gold,
            res.anim_tour_effects,
            res.anim_vip_icon
        ];

        if(GV.THEME == THEME.NORMAL){
            list.push(res.anim_lobby_girl_1);
        }
        else if(GV.THEME == THEME.XMAS){
            list.push(res.anim_lobby_girl_2);
        }

        for(var i = 0; i < list.length; ++i){
            fr.loadAnimationDataByKey(list[i]);
        }
    },

    loadingCallBack:function (obj) {
        ++this._numberOfLoadedSprites;
        //var percent = (this._numberOfLoadedSprites / this._numberOfSprites) * 100;
        //this.lbPercent.setString(percent.toFixed(cc.sys.isNative ? 0 : 2) + '%');
        //this._imgProgressBar.setPercent(percent);

        if (this._numberOfLoadedSprites == this._numberOfSprites) {
            // load json files before init modules
            resourceMgr.loadJsonList(this.loadDone.bind(this));
        }
    },

    /**
     * auto call this function when all texture loaded
     * may be change scene or whatever
     */
    loadDone: function(){
        cc.director.setAnimationInterval(1.0 / GV.FRAME_RATE);

        // load sprite frames
        this.loadPlistFiles();

        // load DragonBones
        this.loadDBAnimations();

        // init module mgr
        moduleMgr = new ModuleMgr();

        // init notification mgr
        notificationMgr.onStart();

        if(GV.MODE == BUILD_MODE.DEV){
            _.delay(function(){
                languageMgr.updateLang();
                sceneMgr.viewSceneById(GV.SCENE_IDS.LOGIN);

                // cheat payment if enable
                if(Cheat.isEnable && Cheat.isEnablePayment) servicesMgr.setListPaymentMethods(Cheat.payments);
            }, 500);
        }
        else{
            // get info from service
            servicesMgr.sendRequest(function(){
                // cheat payment if enable
                if(Cheat.isEnable && Cheat.isEnablePayment) servicesMgr.setListPaymentMethods(Cheat.payments);

                if(fr.portal.loginByCache() == false && fr.portal.loginByPriority() == false){
                    //if(servicesMgr.isUsePortal()){
                    //    fr.NativeService.endGame();
                    //    return;
                    //}

                    // clean last login method in cache
                    fr.UserData.setString(UserDataKey.LOGIN_METHOD, "");
                    sceneMgr.viewSceneById(GV.SCENE_IDS.LOGIN);
                }
            });
        }

        //Test.main();
    },

    onEnter: function(){
        this._super();
        cc.director.setAnimationInterval(1.0 / 60);

        if(cc.sys.isNative){
            var self = this;
            this._imgProgressBar.runPercent(0, 100, 0.6, function(){
                // load json files before init modules
                resourceMgr.loadJsonList(self.loadDone.bind(self));
            });
            //this.loadTextures(res_all_game_textures);
        }
        else {
            this.loadTextures(texture_preload);
            this.loadAnimations(animation_preload);
        }
    },

    onEnterTransitionDidFinish: function(){
        this._super();
        resourceMgr.loadPlist(res.avatar_borders_plist);

        // cheat
        //this.getLayer(GV.LAYERS.BG).setVisible(false);
        //this.getLayer(GV.LAYERS.GAME).setVisible(false);
        //this.getLayer(GV.LAYERS.GUI).setVisible(false);
        //for(var i = 0; i < 100; ++i){
        //    var a = new fr.CircleAvatar("avatar_default", cc.size(104, 105));
        //    a.setPosition(_.random(100, 800),_.random(100, 560));
        //    this.getLayer(GV.LAYERS.CURSOR).addChild(a);
        //}
    },

    onExit: function(){
        this._super();

        cc.textureCache.removeTextureForKey(res.bg_loading);
        cc.textureCache.removeTextureForKey(res.logo_zingplay);
        cc.textureCache.removeTextureForKey(res.bg_progress_bar);
        cc.textureCache.removeTextureForKey(res.progress_bar);

        sceneMgr.removeScene(GV.SCENE_IDS.LOADING);
    },
});
