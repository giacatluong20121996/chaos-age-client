/**
 * Created by DELL on 8/10/2018.
 */

var LoadingModule = BaseModule.extend({
    _className: "LoadingModule",

    ctor: function(moduleId){
        this._super(moduleId);
        this.setListenerValue(5000, 5999);

    },

    //=========================================================
    // RECEIVE
    //=========================================================
    processPackages: function(cmd){
        if(this._curPackage == null){
            cc.warn("%s NOT FOUND curPackage with cmd (%d)", this.getClassName(), cmd);
            return;
        }

        switch (cmd){
            case CMD.LOADING_ALL_INFO_:
                if(this._errorCode == ERROR_CODE.SUCCESS){
                    ZLog.debug('----> loading ok');

                    ZLog.debug('----> to do what you do');
                    // it's just demof
                    // this.sendDemoGetPlayerInfo();

                    // TODO init map

                    cc.log("loading: " + this._curPackage.config);
                    sceneMgr.viewSceneById(GV.SCENE_IDS.PLAY);
                    var json = JSON.parse(this._curPackage.config);
                    mapMgr.load(json["map"]);


                }
                else if(this._errorCode == ERROR_CODE.SESSION_EXPIRED || this._errorCode == ERROR_CODE.SESSION_KEY_INVALID){
                    ZLog.debug("USER_LOGIN: SESSION_EXPIRED or SESSION_KEY_INVALID, error_code = %d", this._errorCode);

                    // TODO get new session key, then re-try
                }
                else{
                    // TODO show error code
                }
                break;


            case CMD.DISCONNECTED:
                if(!connector.isConnected()) break;

                switch (this._errorCode){
                    case DisconnectReason.IDLE:
                        // TODO
                        break;
                    case DisconnectReason.LOGIN:
                        // TODO
                        break;

                    case DisconnectReason.KICK:
                        // TODO
                        break;

                    case DisconnectReason.BAN:
                        // TODO
                        break;

                    case DisconnectReason.HANDSHAKE:
                        // TODO
                        break;

                    case DisconnectReason.UNKNOWN:
                    default:
                        // TODO
                        break;
                }

                connector.disconnect();
                break;


            default :
                break;
        }
    },
    createReceivedPackage: function(cmd, pkg){
        var pk = null;
        switch (cmd){
            case CMD.LOADING_ALL_INFO_:
                pk = this.getInPacket(CmdLoadingAllInfo);
                break;
            case CMD.DISCONNECTED:
                pk = this.getInPacket(CmdReceiveDisconnect);
                break;
            default :
                break;
        }

        return pk;
    },

    //=========================================================

    //=========================================================
    // SEND
    //=========================================================
    sendLoadingSignal: function(idUser){
        ZLog.debug("--------------SEND LOADING signal---------------");

        var pk = this.getOutPacket(CmdSendLoadingSignal, idUser);
        this.send(pk);
    },

});

DisconnectReason = {
    IDLE: 0,
    KICK: 1,
    BAN: 2,
    LOGIN: 3,
    UNKNOWN: 4,
    HANDSHAKE: 5
};
