/**
 * Created by VitaminB on 11/19/2017.
 */

var CmdSendDemoGetPlayerInfo = BaseOutPacket.extend({

    ctor: function () {
        this._super();
        this.initData(2);
        this.setCmdId(CMD.DEMO_PLAYER_GET_INFO);
    }
});