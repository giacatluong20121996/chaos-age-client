/**
 * Created by pc1 on 8/3/2018.
 */
//var f = 18.53;
var f = 20;
var d = 50;
var scale = 1;
var num = 80;

var Camera = cc.Class.extend({
    _className: "Camera",
    x: 0,
    y: 0,
    w: 0,
    h: 0,
    ctor: function(){

        this.init();
    },

    init: function(){

    },

    updatePosition: function(isoCam){
        var matrix = Multify(isoMap, Inverse(isoCam));
        var p = Multify(matrix, [[0],[0],[1]]);
        this.x = p[0][0];
        this.y = p[1][0];
        p = Multify(matrix, [[cc.winSize.width],[cc.winSize.height],[1]]);
        this.w = p[0][0] - this.x;
        this.h = p[1][0] - this.y;
    },

    outOfScreen: function(vx, vy){
        return ((this.x < 0 &&  vx > 0)
                || (this.y < 0 && vy > 0)
                || (this.x + this.w > 1600 && vx < 0)
                || (this.y + this.h > 1600/1.28 && vy < 0));
    }

});

var TestMap = BaseScene.extend({
    _className: "TestMap",
    _matrix: null,
    _vx: 0,
    _vy: 0,
    _ax: 0,
    _ay: 0,
    _desX: 0,
    _desY: 0,
    _startX: 0,
    _startY: 0,
    _isCreate: false,
    _camera: null,

    ctor: function(){
        this._super();

        this._camera = new Camera();
        this.init();
    },

    init: function(){
        this._super();
        this.calculateMatrix();
        this.drawGrid();
        this.drawButton();
        //this.drawMap();
        //this.schedule(this.zoom,2);
        this.move();

        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan,
            onTouchMoved: this.onTouchMoved,
            onTouchEnded: this.onTouchEnded
        }, this);
    },

    onTouchBegan: function(touch, event){
        var target = event.getCurrentTarget();
        target._startX = touch.getLocationX();
        target._startY = touch.getLocationY();
        return true;
    },

    onTouchMoved: function(touch, event){
        var target = event.getCurrentTarget();

        var location = touch.getLocation();
        target._desX = location.x;
        target._desY = location.y;

        var delX = location.x - target._startX;
        var delY = location.y - target._startY;

        var h = Math.cos(Math.atan(delX/delY))*(delY > 0 ? 1 : -1);
        target._vy = 500 * h;
        target._vx = Math.sqrt(500*500 - target._vy*target._vy)*(delX > 0 ? 1 : -1);
        target._ay = - 300 * h;
        target._ax = - Math.sqrt(300*300 - target._ay*target._ay)*(delX > 0 ? 1 : -1);
    },

    onTouchEnded: function(touch, event){
    },

    drawButton: function(){
        var button = new ccui.Button();
        button.setTouchEnabled(true);
        button.loadTextures(res.btn_normal, res.btn_press, res.btn_disable);
        button.x = 100;
        button.y = 100;
        button.addTouchEventListener(this.touchEvent, this);
        this.addChild(button);
    },

    touchEvent: function(sender, type){
        if(this._isCreate) return;
        this._isCreate = true;
        var building = new TestBuilding("res/texture/3.png");

        var p = [[building._xMap, building._yMap, 1]];
        p = Multify(this._matrix, T(p));

        building.setPosition(p[0][0], p[1][0]);
        this.addChild(building);

        var npc = new TestNPC("res/texture/ball.png");
        this.addChild(npc);
        var grid = new Array(num);
        for(var i = 0; i < num; i++){
            grid[i] = new Array(num);
            for(var j = 0; j < num; j++){
                if(i == 0 || i == num-1 || j == 0 || j == num-1)
                    grid[i][j] = 0;
                else
                    grid[i][j] = 1;
            }
        }

        for(var j = building._xMap - building._wMap/2 + 1; j < building._xMap + building._wMap/2 + 1; j++) {
            for (var i = building._yMap - building._hMap / 2 + 1; i < building._yMap + building._hMap / 2 + 1; i++) {
                grid[j][i] = 0;
            }
        }
        //for(var k = 0 ; k < 80; k++){
        //    cc.log(grid[k]);
        //}
        var graph = new Graph(grid, {diagonal: true});
        npc.move(graph ,new GridNode(3,4,1), new GridNode(20,20,1));
    },

    move: function(){
        this.scheduleUpdate();
    },

    stopMoving: function(){
        this._isMoving = false;
        this.unscheduleUpdate();
    },

    update: function(dt){
        if(Math.abs(this._startX - this._desX) < 10 && Math.abs(this._startY - this._desY) < 10){
            this._vx = 0;
            this._vy = 0;
        }
        if(this._camera.outOfScreen(this._vx, this._vy)){
            this._vx = 0;
            this._vy = 0;
        }
        if(Math.abs(this._vx) < 10 && Math.abs(this._vy) < 10) {
            this._ax = 0;
            this._ay = 0;
            return;
        }
        this._vx += this._ax*dt;
        this._vy += this._ay*dt;
        this._startX += this._vx*dt;
        this._startY += this._vy*dt;
        this._matrix = Multify(transformMatrix(-this._vx*dt, -this._vy*dt), this._matrix);
        this._camera.updatePosition(this._matrix);
        this.updateGraph();
    },

    zoom: function(){
        scale+=0.1;
        this.calculateMatrix();
        this.updateGraph();
    },

    updateGraph: function(){
        this.removeAllChildren(true);
        this.drawGrid();
        this.drawButton();
        this._isCreate = false;
        //this.drawMap();
    },

    calculateMatrix: function(){
        var iso2D = Multify(Multify(Multify(Multify(Multify(Multify(a7,a6),a1),a2),a3),a4),a5);
        cc.log(Multify(iso2D, [[0],[0],[0]]));
        cc.log(Multify(iso2D, [[80],[0],[0]]));
        cc.log(Multify(iso2D, [[0],[80],[0]]));

        iso2D[2][2] = 1;
        cc.log(iso2D);
        isoMap = Multify(a8,iso2D);
        cc.log(isoMap);
        this._matrix = Multify(transformMatrix(0,0, scale),isoMap);
        this._camera.updatePosition(this._matrix);
    },

    drawGrid: function(){
        var x = [[0,0,1]];
        var y = [[0,0,1]];
        for(var i = 0; i <= num; i+=2){
            x[0][1] = i;
            y[0][0] = num;
            y[0][1] = i;
            var xp = Multify(this._matrix, T(x));
            var yp = Multify(this._matrix, T(y));
            var drawNode = new cc.DrawNode();
            drawNode.drawSegment(cc.p(xp[0][0],xp[1][0]), cc.p(yp[0][0],yp[1][0]),1,cc.Color(250,0,0,255));
            this.addChild(drawNode);
        }

        var x = [[0,0,1]];
        var y = [[0,0,1]];
        for(var i = 0; i <= num; i+=2){
            x[0][0] = i;
            y[0][1] = num;
            y[0][0] = i;
            var xp = Multify(this._matrix, T(x));
            var yp = Multify(this._matrix, T(y));
            var drawNode = new cc.DrawNode();
            drawNode.drawSegment(cc.p(xp[0][0],xp[1][0]), cc.p(yp[0][0],yp[1][0]),1,cc.Color(250,0,0,255));
            this.addChild(drawNode);
        }
    },

    drawMap: function(){
        var bg = cc.Sprite.create(res.TEST_BG_2);
        var p = [[5,2,1]];
        p = Multify(this._matrix,T(p));
        bg.setPosition(p[0][0],p[1][0]);
        bg.setScaleX(scale);
        bg.setScaleY(scale);
        this.addChild(bg);

        var bg = cc.Sprite.create(res.TEST_BG_3);
        var p = [[7.5,2.5,1]];
        p = Multify(this._matrix,T(p));
        bg.setPosition(p[0][0],p[1][0]);
        bg.setScaleX(scale);
        bg.setScaleY(scale);
        this.addChild(bg);
    }
});

//var iso2D = [[9.374999999826384,-9.374999999826384,-6.6291260735625],
//    [6.928236549221171,6.928236549221171,4.898932364985512],
//    [0,0,1]];
//
var isoMap = [[9.374999999826384,-9.374999999826384,793.3708739264375],
    [6.928236549221171,6.928236549221171,54.89893236498551],
    [0,0,1]];


//3D -> 2D
alpha = (35.264*Math.PI)/180;

var a1 = [[1,0,0],[0,1,0], [0,0,0]];

var a2 = [[1, 0, 0],[0, Math.cos(alpha), Math.sin(alpha)],[0, -Math.sin(alpha), Math.cos(alpha)]];

var a3 = [[0.70710678118, 0, -0.70710678118],[0,1,0],[0.70710678118, 0, 0.70710678118]];

var a4 = [[1,0,0],[0,1,0],[-1,0,1]];

var a5 = [[0.70710678118, -0.70710678118, 0],[0.70710678118, 0.70710678118, 0],[0,0,1]];

//custom scale
var a6 = [[1,0,0],[0,1.2976162482,0],[0,0,1]];

//Scale to screen pixel
var a7 = [[f/2,0,0],[0,f/2,0],[0,0,1]];

//2D -> Map
var a8 = [[1,0,805],[0,1,23],[0,0,1]];

//Play
//Screen move
var a9 = [[1,0,-1600],[0,1,-200],[0,0,1]];

//Screen zoom O
var a10 = [[scale,0,0],[0,scale,0],[0,0,1]];

//Screen zoom X
var a11 = [[scale,0,-scale*800],[0,scale,-scale*100],[0,0,1]];


var transformMatrix = function(x,y,scale){
  var m = [[1,0,0],[0,1,0],[0,0,1]];
    scale = scale || 1;
    m[0][0] = scale;
    m[0][2] = -scale*x;
    m[1][1] = scale;
    m[1][2] = -scale*y;
    return m;
};

var T = function(x){
    var rows = x.length, cols = x[0].length,
        m = new Array(cols);
    for(var r = 0; r < cols; r++){
        m[r] = new Array(rows);
        for(var c = 0; c < rows; c++){
            m[r][c] = x[c][r];
        }
    }
    return m;
};

var Multify = function(a, b){
    var aNumRows = a.length, aNumCols = a[0].length, bNumCols = b[0].length,
        m = new Array(aNumRows);
    for (var r = 0; r < aNumRows; ++r) {
        m[r] = new Array(bNumCols);
        for (var c = 0; c < bNumCols; ++c) {
            m[r][c] = 0;
            for (var i = 0; i < aNumCols; ++i) {
                m[r][c] += a[r][i] * b[i][c];
            }
        }
    }
    return m;
};

function Reduction(a, size, pivot, col) {
    var i, j;
    var factor;
    factor = a[pivot][col];

    for (i = 0; i < 2 * size; i++) {
        a[pivot][i] /= factor;
    }
    for (i = 0; i < size; i++) {
        if (i != pivot) {
            factor = a[i][col];
            for (j = 0; j < 2 * size; j++) {
                a[i][j] = a[i][j] - a[pivot][j] * factor;
            }
        }
    }
};

function Inverse(matrix){
    matrix = T(matrix.concat([[1,0,0],[0,1,0],[0,0,1]]));
    for (var i = 0; i < 3; i++) {
        Reduction(matrix, 3, i, i);
    }
    return T(matrix).slice(3,6);
}

