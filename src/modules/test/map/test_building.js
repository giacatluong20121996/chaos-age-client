/**
 * Created by pc1 on 8/6/2018.
 */

var TestBuilding = cc.Sprite.extend({
    _nameClass : "TestBuilding",
    _xMap: 11,
    _yMap: 11,
    _hMap: 6,
    _wMap: 6,//80x80

    ctor: function(texture){
        this._super(texture);

        this.init();
    },

    init: function(){
        cc.eventManager.addListener({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            swallowTouches: true,
            onTouchBegan: this.onTouchBegan,
            onTouchMoved: this.onTouchMoved,
            onTouchEnded: this.onTouchEnded
        },this);
    },

    onTouchBegan: function(touch, event){
        var target = event.getCurrentTarget();
        var rect = target.getBoundingBox();
        //Check the click area
        if (cc.rectContainsPoint(rect, touch.getLocation())) {
            target.opacity = 180;
            return true;
        }
        return false;
    },

    onTouchMoved: function(touch, event){
        var target = event.getCurrentTarget();
        var parent = target.getParent();

        var matrix = Inverse(parent._matrix);

        var p = [[touch.getLocationX()], [touch.getLocationY()], [1]];
        p = Multify(matrix, p);

        p = [[Math.round(p[0][0]), Math.round(p[1][0]), 1]];

        if(Math.abs(p[0][0] - target._xMap) == 2) {
            target._xMap =  p[0][0];
            target.updatePosition();
        }
        if(Math.abs(p[0][1] - target._yMap) == 2) {
            target._yMap = p[0][1];
            target.updatePosition();
        }
    },

    updatePosition: function(){
        var p = [[this._xMap],[this._yMap], [1]];
        p = Multify(this.getParent()._matrix, p);
        this.setPosition(p[0][0], p[1][0]);
    },

    onTouchEnded: function(touch, event){
        var target = event.getCurrentTarget();
        target.opacity = 1000;
    },
});
