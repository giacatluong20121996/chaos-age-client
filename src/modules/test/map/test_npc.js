/**
 * Created by Minh Nhi on 8/6/2018.
 */

/**
 * Created by pc1 on 8/6/2018.
 */

var TestNPC = cc.Sprite.extend({
    _nameClass : "TestNPC",

    ctor: function(texture){
        this._super(texture);

        this.init();
    },

    init: function(){

    },

    move: function(graph, start, goal){
        var path = astar.search(graph, start, goal, {closest: true, heuristic: astar.heuristics.diagonal});
        var sprite_action = [];
        for(var i = 0; i < path.length; i++){
            var p = [[path[i].x],[path[i].y], [1]];
            p = Multify(this.getParent()._matrix, p);
            sprite_action.push(cc.MoveTo.create(0.5,cc.p(p[0][0], p[1][0])));
        }

        var sequence = cc.Sequence.create(sprite_action);
        this.runAction(sequence);
    }

});

