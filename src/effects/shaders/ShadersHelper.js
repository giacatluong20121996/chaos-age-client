/**
 * Created by pc1 on 8/13/2018.
 */

var ShadersHelper = cc.Class.extend({
    _nameClass: "ShadersHelper",
    _shaders: [],
    ctor: function(){

    },

    getShaders: function(id){
        if(this._shaders[id]){
            return this._shaders[id];
        }else{
            var shaders = ShadersHelper.createShaders(id);
            this._shaders[id] = shaders;
            return shaders;
        }
    }

});

ShadersHelper.TYPE = {
    DEFAULT_SHADER      : 0,
    GRAYSCALE_SHADERS   : 1
};

ShadersHelper.createShaders = function(id){
    switch (id){
        case ShadersHelper.TYPE.DEFAULT_SHADER:
            var sprite = cc.Sprite.create("res/texture/2.png");
            return sprite.getShaderProgram();
        case ShadersHelper.TYPE.GRAYSCALE_SHADERS:
            var shader = cc.GLProgram.create("res/shaders/gray.vsh", "res/shaders/gray.fsh");
            shader.retain();
            shader.addAttribute(cc.ATTRIBUTE_NAME_POSITION, cc.VERTEX_ATTRIB_POSITION);
            shader.addAttribute(cc.ATTRIBUTE_NAME_COLOR, cc.VERTEX_ATTRIB_COLOR);
            shader.addAttribute(cc.ATTRIBUTE_NAME_TEX_COORD, cc.VERTEX_ATTRIB_TEX_COORDS);
            shader.link();
            shader.updateUniforms();
            return shader;
    }
};

