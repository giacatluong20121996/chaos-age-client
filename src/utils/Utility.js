/**
 * Created by bachbv on 1/10/2017.
 */

var Utility = Utility || {};

Utility.getCmdKey = function(cmd){
    for(var key in CMD){
        if(cmd === CMD[key]){
            return key;
        }
    }
};