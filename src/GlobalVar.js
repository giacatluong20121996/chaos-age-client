/**
 * Created by bachbv on 1/9/2017.
 */

var GV = GV || {};

GV.IP                       = "127.0.0.1";
GV.PORT                     = cc.sys.isNative ? "1101" : "8081";
GV.GAME                     = "fresher";
BUILD_MODE = {
    DEV:                    "dev",
    PRIVATE:                "private",
    LIVE:                   "live"
};
MAX_DESIGN_SIZE = cc.size(1136, 640);

//==============================================
// modify when build
//==============================================
GV.MODE                     = BUILD_MODE.DEV;
GV.VERSION_NAME             = "1.0.1a";
GV.VERSION                  = "1";
//==============================================

//==============================================
// MANAGERS
//==============================================
var moduleMgr               = null;
var languageMgr             = null;
var sceneMgr                = null;
var connector               = null;

var mapMgr                  = null;
var camera                  = null;
var shadersHelper           = null;

var itemShopMgr             = null;

var movementBgMgr           = null;

var loginScene              = null;
var registerScene           = null;
var layerMap                = null;

//==============================================
// SCENE IDS
//==============================================
GV.SCENE_IDS = {
    LOADING:                0,
    LOGIN:                  1,
    PLAY:                   2,
    SHOP:                   3,
    ITEM_SHOP:              4
};

//==============================================
// TYPE LOGIN
//==============================================
GV.TYPE_LOGIN = {
    LOGIN:                  0,
    REGISTER:               1,
};

//==============================================
// OBJECT IDs
//==============================================
GV.OBJECT_IDS = {
    TOW_1:                  "TOW_1",
    RES_1:                  "RES_1",
    RES_2:                  "RES_2",
    AMC_1:                  "AMC_1",
};

//==============================================
// LAYERS
//==============================================
GV.MAX_LAYERS = 8;
GV.LAYERS = {
    BG:                     0,
    GAME:                   1,
    EFFECT:                 2,
    MOVE:                   3,  // special layer, this includes : layer BG, GAME, EFFECT
    GUI:                    4,
    GUI_EFFECT:             5,
    LOADING:                6,
    CURSOR:                 7,
};

CONNECTION_STATUS = {
    NO_NETWORK:             0,
    THREE_G:                1,
    WIFI:                   2
};

GV.RES_TYPE = {
    GOLD:                   0,
    ELIXIR:                 1,
    G:                      2
};

GV.BUILDING_RES_TYPE = {
    GOLD:                   0,
    ELIXIR:                 1,
};

GV.SHOP_CATEGORY = {
    RES:                    0,
    ARMY:                   1
};

GV.OBJECT_IDS = {
    TOW_1:           "TOW_1",
    RES_1:           "RES_1",
    RES_2:           "RES_2"
};