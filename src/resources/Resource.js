var res = {
	//=====================================================
	// loading
	//=====================================================
	bg_loading: "res/texture/loading/bg_loading.png",
	bg_progress_bar: "res/texture/loading/bg_progress_bar.png",
	img_progress_bar_header: "res/texture/loading/img_progress_bar_header.png",
	logo_zingplay: "res/texture/loading/logo_zingplay.png",
	progress_bar: "res/texture/loading/progress_bar.png",

	btn_bg_orange: "res/texture/btn_bg_orange.png",

	//=====================================================
	// animations
	//=====================================================
	anim_btn_quick_play: "btn_quick_play",
	anim_logo_gsn: "logo_gsn",

	//=====================================================
	// effects
	//=====================================================
	res_1_1_plist: "animations/building-effects/res_1_1.plist",
	res_1_1:	"animations/building-effects/res_1_1.png",
	//=====================================================
	// audio common
	//=====================================================
	button_click: "res/audio/common/button_click.mp3",
	music_ingame: "res/audio/common/music_ingame.mp3",
	music_lobby: "res/audio/common/music_lobby.mp3",

	//=====================================================
	// fonts
	//=====================================================
	NBM_40: "res/fonts/nbm_40.fnt",
	NBM_TRANSIS: "res/fonts/nbm_transis.fnt",
	NUMBER_BITMAP_10: "res/fonts/number_bitmap_10.fnt",
	NUMBER_BITMAP_14: "res/fonts/number_bitmap_14.fnt",
	NUMBER_BITMAP_16: "res/fonts/number_bitmap_16.fnt",
	NUMBER_BITMAP_18: "res/fonts/number_bitmap_18.fnt",
	NUMBER_BITMAP_20: "res/fonts/number_bitmap_20.fnt",
	NUMBER_BITMAP_6: "res/fonts/number_bitmap_6.fnt",
	NUMBER_BITMAP_8: "res/fonts/number_bitmap_8.fnt",
	UTM_SWISS_721_BLACK_CONDENSED: "res/fonts/UTM_Swiss_721_Black_Condensed.ttf",
	UTM_SWISS_CONDENSEDBOLD: "res/fonts/UTM_Swiss_CondensedBold.ttf",

	//=====================================================
	// studio
	//=====================================================
	scene_loading: "res/ui/scene_loading.json",

	//end_reader

	//=====================================================
	// test
	//=====================================================
	TEST_BG_1: "res/texture/test/map/1.png",
	TEST_BG_2: "res/texture/test/map/2.png",
	TEST_BG_3: "res/texture/test/map/3.png",
	TEST_BG_4: "res/texture/test/map/4.png",

	s_ball: "res/texture/ball.png",
	s_paddle: "res/texture/paddle.png",

	btn_normal: "res/Default/Button_Normal.png",
	btn_press: "res/Default/Button_Press.png",
	btn_disable: "res/Default/Button_Disable.png",

	res_map: "res/ui/MainScene.json",
	res_info: "res/ui/InfoScene.json",
	res_shop: "res/ui/ShopScene.json",
	res_item_shop:	"res/ui/ItemShopScene.json",

	//=====================================================
	// decorator
	//=====================================================
	grass: "res/Art/Map/map_obj_bg/BG_0/",
	arrow: "res/Art/Map/map_obj_bg/BG/arrowmove",
	bg_green_move: "res/Art/Map/map_obj_bg/BG/GREEN_",
	bg_red_move:	"res/Art/Map/map_obj_bg/BG/RED_",
	//=====================================================
	// shops
	//=====================================================
	item_shop_res_1:	"res/Art/GUIs/icons/shop_gui/icon/RES_1.png",
	item_shop_res_2:	"res/Art/GUIs/icons/shop_gui/icon/RES_2.png",
	item_shop_icon_res_1:	"res/Art/GUIs/Main_Gui/gold_icon.png",
	item_shop_icon_res_2:	"res/Art/GUIs/Main_Gui/elixir_icon.png",

	//=====================================================
	// Config json
	//=====================================================
	RES: "res/config/Resource.json",
	TOW: "res/config/TownHall.json",
	AMC: "res/config/ArmyCamp.json"
}