/**
 * Created by bachbv on 1/22/2017.
 */

var Connector = cc.Class.extend(
    {
        ctor: function () {
            this.config = null;
            this.loadConfig();
            this._isConnected = false;

            this._clientListener = new ConnectorListener();

            this._tcpClient = fr.GsnClient.create();
            this._tcpClient.setFinishConnectListener(this._clientListener.onFinishConnect.bind(this._clientListener));
            this._tcpClient.setDisconnectListener(this._clientListener.onDisconnected.bind(this._clientListener));
            this._tcpClient.setReceiveDataListener(this._clientListener.onReceived.bind(this._clientListener));

            return true;
        },

        getListener: function () {
            return this._clientListener;
        },

        isConnected: function () {
            return this._isConnected;
        },

        loadConfig: function () {
            var commonData = null;
            var fileName = cc.loader.resPath + "res/ipConfig.json";
            if (cc.sys.isNative && !jsb.fileUtils.isFileExist(fileName)) {
                ZLog.debug("File config not exist!!!!");
                return null;
            }

            cc.loader.loadJson(fileName, function (error, jsonData) {
                if (error != null) {
                    ZLog.debug("Load ip config error");
                }
                else {
                    if(GV.MODE == BUILD_MODE.PRIVATE){
                        commonData = jsonData["private"];
                    }
                    else{
                        commonData = jsonData["live"];
                    }

                    this.config = commonData;
                    ZLog.debug("loaded config connector | " + JSON.stringify(this.config));
                }
            }.bind(this));
        },

        getServerInfo: function () {
            return this._serverName + ":" + this._port;
        },

        getNetwork: function () {
            return this._tcpClient;
        },

        connect: function () {
            if (GV.MODE == BUILD_MODE.DEV) {
                this._serverName = GV.IP;
                this._port = GV.PORT;
            }
            else if(GV.MODE == BUILD_MODE.PRIVATE){
                this._serverName = this.config.server;
                this._port = cc.sys.isNative ? this.config.port : this.config.socketport;
            }

            ZLog.debug("----> Connecting to server: " + this._serverName + ":" + this._port);
            this._tcpClient.connect(this._serverName, this._port);
        },

        disconnect: function(){
            this._isConnected = false;

            if(!cc.sys.isNative)
                this._tcpClient.disconnect();
        }
    }
);