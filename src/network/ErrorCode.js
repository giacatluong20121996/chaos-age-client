/**
 * Created by bachbv on 1/21/2017.
 */

var ERROR_CODE = ERROR_CODE || {};

ERROR_CODE = {
    SUCCESS:                                    0,
    FAIL:                                       1,
    PARAM_INVALID:                              2,
    MAINTAIN_SYSTEM:                            3,
    SESSION_KEY_INVALID:                        4,
    SESSION_EXPIRED:                            5
};
