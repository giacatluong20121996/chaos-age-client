#ifndef ScreenFlash_h__
#define ScreenFlash_h__

#include "cocos2d.h"
#include "DragonBonesHeaders.h"
#include "renderer/DBCCRenderHeaders.h"

USING_NS_CC;
USING_NS_DB;

class ScreenFlash :
	public cocos2d::CCLayerColor
{

public:
	CREATE_FUNC(ScreenFlash);
	ScreenFlash();
	~ScreenFlash();
	bool init();
	void startGame();

private:
	virtual void update(float delta) override;
private:
	DBCCArmatureNode* _logoGSNArmatureNode;
};


#endif // ScreenFlash_h__
