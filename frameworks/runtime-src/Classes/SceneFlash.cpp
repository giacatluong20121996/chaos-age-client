#include "SceneFlash.h"
#include "js_module_register.h"

USING_NS_CC;

ScreenFlash::ScreenFlash() 
	: _logoGSNArmatureNode(nullptr)
{

}

ScreenFlash::~ScreenFlash()
{
	_logoGSNArmatureNode->removeFromParentAndCleanup(true);
}

bool ScreenFlash::init()
{
	if (!LayerColor::initWithColor(Color4B(255, 255, 255, 255)))
	{
		return false;
	}

	// load anim logo
	DBCCFactory::getInstance()->loadDragonBonesData("res/animations/logo_gsn/skeleton.xml", "logo_gsn");
	DBCCFactory::getInstance()->loadTextureAtlas("res/animations/logo_gsn/texture.plist", "logo_gsn");

	// create armature
	_logoGSNArmatureNode = DBCCFactory::getInstance()->buildArmatureNode("logo_gsn");
	_logoGSNArmatureNode->setPosition(Vec2(Director::getInstance()->getVisibleSize() * 0.5));
	_logoGSNArmatureNode->setCascadeOpacityEnabled(true);
	_logoGSNArmatureNode->setOpacity(0);
	this->addChild(_logoGSNArmatureNode);

	auto action = Sequence::create(		
		Spawn::create(
			FadeIn::create(1),
			CallFunc::create(
				[&]() { // lamda					
					_logoGSNArmatureNode->getAnimation()->gotoAndPlay("run", -1, -1, 1);
				}
			),
			nullptr
		),
		DelayTime::create(1.5),
		CallFunc::create(
			[&]() { // lamda
				startGame();
			}
		),
		nullptr);

	_logoGSNArmatureNode->runAction(action);

	return true;
}

void ScreenFlash::startGame(){
	js_module_register();
	ScriptingCore* sc = ScriptingCore::getInstance();
	sc->start();
	sc->runScript("script/jsb_boot.js");

	ScriptEngineProtocol *engine = ScriptingCore::getInstance();
	ScriptEngineManager::getInstance()->setScriptEngine(engine);
	ScriptingCore::getInstance()->runScript("main.js");
}

void ScreenFlash::update(float delta)
{

}